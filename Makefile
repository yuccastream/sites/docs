.DEFAULT_GOAL := help

.PHONY: help
help: ## Справка по командам
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

.PHONY: build
build: ## Build docker image
	@echo '\e[0;32m'"==> Build docker image"'\e[0m'
	./build.sh build

.PHONY: push
push: ## Push docker image
	./build.sh push

.PHONY: serve-ru
serve-ru: ## Локальный запуск RU версии
	docker run --rm -it -v $(PWD):/docs -w /docs -p 8000:8000 squidfunk/mkdocs-material:9.5.25 serve -f ./ru/mkdocs.yml -a 0.0.0.0:8000
	

.PHONY: serve-en
serve-en: ## Локальный запуск EN версии
	docker run --rm -it -v $(PWD):/docs -w /docs -p 8000:8000 squidfunk/mkdocs-material:9.5.25 serve -f ./en/mkdocs.yml -a 0.0.0.0:8000

.PHONY: versioned_pages
versioned_pages: ## Генерация старой версии конфигурации
	go run ./cmd/versioned_pages -c versioned_pages.yaml
