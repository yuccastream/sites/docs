package main

import (
	"os"

	"gopkg.in/yaml.v2"
)

type Config struct {
	VersionedPages    map[string]Page      `yaml:"versioned_pages"`
	VersionedNotices  map[string]string    `yaml:"versioned_notices"`
	VersionedReplaces []Replace            `yaml:"versioned_replaces"`
	FileReplaces      map[string][]Replace `yaml:"file_replaces"`
}

type Replace struct {
	From string `yaml:"from"`
	To   string `yaml:"to"`
}

type Page struct {
	Name      string            `yaml:"name"`
	Ref       string            `yaml:"ref"`
	AddNotice bool              `yaml:"add_notice"`
	Files     map[string]string `yaml:"files"`
}

func LoadFromFile(filename string) (*Config, error) {
	b, err := os.ReadFile(filename)
	if err != nil {
		return nil, err
	}
	c := &Config{}
	err = yaml.Unmarshal(b, c)
	if err != nil {
		return nil, err
	}
	for name, page := range c.VersionedPages {
		if page.Name == "" {
			page.Name = name
		}
		c.VersionedPages[name] = page
	}
	return c, nil
}
