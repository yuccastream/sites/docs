package main

import (
	"fmt"
	"os/exec"
)

func GetFileContent(ref, filename string) ([]byte, error) {
	cmd := exec.Command("git", "show", fmt.Sprintf("%s:%s", ref, filename))
	body, err := cmd.CombinedOutput()
	if err != nil {
		return nil, fmt.Errorf("%v: %s", err, string(body))
	}
	return body, nil
}
