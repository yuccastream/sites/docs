package main

import (
	"bytes"
	"flag"
	"fmt"
	"os"
	"text/template"
)

var (
	configFile = flag.String("c", "mkdocs.yml", "Path to configuration file")
)

func main() {
	err := realMain()
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}

func realMain() error {
	flag.Parse()

	config, err := LoadFromFile(*configFile)
	if err != nil {
		return err
	}

	for name, page := range config.VersionedPages {
		fmt.Printf("%s (%s)\n", name, page.Ref)
		for src, dest := range page.Files {
			fmt.Printf("%s -> %s\n", src, dest)
			b, err := GetFileContent(page.Ref, src)
			if err != nil {
				return err
			}
			notice, ok := config.VersionedNotices[src]
			if page.AddNotice && ok {
				notice, err := TemplateText(notice, page)
				if err == nil {
					b = append([]byte(notice+"\n"), b...)
				}
			}
			for _, replace := range config.VersionedReplaces {
				replaceTo, err := TemplateText(replace.To, page)
				if err != nil {
					continue
				}
				b = bytes.Replace(b, []byte(replace.From), []byte(replaceTo), -1)
			}
			err = os.WriteFile(dest, b, 0644)
			if err != nil {
				return err
			}
		}
	}

	for filename, replaces := range config.FileReplaces {
		b, err := os.ReadFile(filename)
		if err != nil {
			continue
		}
		for _, replace := range replaces {
			b = bytes.Replace(b, []byte(replace.From), []byte(replace.To), -1)
		}
		err = os.WriteFile(filename, b, 0644)
		if err != nil {
			continue
		}
	}

	return nil
}

func TemplateText(text string, data interface{}) (string, error) {
	t, err := template.New("template").Parse(text)
	if err != nil {
		return "", err
	}
	var tpl bytes.Buffer
	err = t.Execute(&tpl, data)
	if err != nil {
		return "", err
	}
	return tpl.String(), nil
}
