---
title: Welcome
description: Video surveillance for home and business
---

# Welcome

You are on the site with the documentation of the video recorder [Yucca](https://yucca.app) - software for organizing video surveillance.

## Quick start

Installation and system requirements:

- [Installation](/en/install/)

## Mobile app

<a href='https://play.google.com/store/apps/details?id=app.yucca.android&utm_source=https%3A%2F%2Fdocs.yucca.app'><img alt='Get it on Google Play' src='/en/media/google-play-badge-en.png' width="300"/></a>
<a href='https://releases.yucca.app/android/latest/app-yucca-release.apk'><img alt='Download APK' src='/en/media/Download_Android_APK_Badge.png' width="300" /></a>

## Improve translation of Yucca

If you see a typo or translation error, please [suggest a correction](https://weblate.yucca.app/engage/yucca/).

## Found a mistake in the text?

If you see an error or typo, help fix it - write to us [by mail](mailto:info@yucca.app), in [Telegram-chat](https://t.me/yuccastream).
