---
title: Installation for Debian/Ubuntu
description: Video surveillance for home and business
---

# Installation for Debian/Ubuntu

!!! info "Support"
    If you need expert help with this or any other issue, please see our terms and conditions **[technical support](/en/support/)**.

!!! note "Use PostgreSQL, with a large number of cameras"
    When operating in production environments with a large number of cameras
    and motion detection enabled, we recommend using PostgreSQL as a more productive database.
    How to install and configure usage, read [here](/en/usage/DB/postgres/)

??? question "Which version should I choose?"
    There are 2 editions of Yucca

    === "Free"
         The completely free version does not require a license and does not contain advanced features.

    === "Plus/Enterprise"
         Contains advanced features and requires the purchase and use of a license.  
         If you have a **Plus** or **Enterprise** license, install yucca with the `ent` postfix.

Add repository and update package list:

```sh
(
sudo apt-get update
sudo apt-get install -y ca-certificates curl gnupg
curl -fsSL https://repo.yucca.app/apt/gpg.key | sudo gpg --dearmor -o /etc/apt/trusted.gpg.d/yucca.gpg
echo 'deb [signed-by=/etc/apt/trusted.gpg.d/yucca.gpg] https://repo.yucca.app/apt/ /' | sudo tee /etc/apt/sources.list.d/yucca.list
sudo apt update
)
```

Install **Free** or **Ent** version of Yucca

=== "Free"

    ```sh
    sudo apt install yucca
    ```

=== "Plus/Enterprise"

    ```sh
    sudo apt install yucca-ent
    ```

After launch, the Web interface will be available at http://ip-your-server:9910

## Additional

* [System requirements](/en/usage/SystemRequirements/)
* [Configuration](/en/configuration/)
