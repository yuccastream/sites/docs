---
title: Installation for Docker
description: Video surveillance for home and business
---

# Installation for Docker

!!! info "Support"
    If you need expert help with this or any other issue, please see our terms and conditions **[technical support](/en/support/)**.

!!! note "Use PostgreSQL, with a large number of cameras"
    When operating in production environments with a large number of cameras
    and motion detection enabled, we recommend using PostgreSQL as a more productive database.
    How to install and configure usage, read [here](/en/usage/DB/postgres/)

??? question "Which version should I choose?"
    There are 2 editions of Yucca

    === "Free"
         The completely free version does not require a license and does not contain advanced features.

    === "Plus/Enterprise"
         Contains advanced features and requires the purchase and use of a license.  
         If you have a **Plus** or **Enterprise** license, install yucca with the `ent` postfix.

## Images

All tags here [yuccastream/yucca](https://hub.docker.com/r/yuccastream/yucca)

=== "Free"

    ```sh
    yuccastream/yucca:latest
    ```

=== "Plus/Enterprise"

    ```sh
    yuccastream/yucca:latest-ent
    ```

## Example with docker-compose

!!! note "Dependencies"
    Install [Docker](https://docs.docker.com/engine/install/)

    ```shell
    bash <(curl -sSL https://get.docker.com/)
    ```

Copy file `docker-compose.yml`:

=== "Free"

    ```sh
    tee docker-compose.yml <<EOF
    ---
    version: "3.8"

    networks:
      yucca_network:

    volumes:
      yucca_data:
      yucca_ffmpeg:

    services:
      yucca:
        image: yuccastream/yucca:latest
        restart: always
        volumes:
          - "yucca_data:/opt/yucca/data"
          - "yucca_ffmpeg:/opt/yucca/ffmpeg"
        networks:
          - yucca_network
        ports:
          - 9910:9910 # Web UI
          - 9911:9911 # Pprof
          - 9912:9912 # Telemetry
          - 1025:1025 # SMTP server
    EOF
    ```

=== "Plus/Enterprise"

    ```sh
    tee docker-compose.yml <<EOF
    ---
    version: "3.8"

    networks:
      yucca_network:

    volumes:
      yucca_data:
      yucca_ffmpeg:

    services:
      yucca:
        image: yuccastream/yucca:latest-ent
        restart: always
        volumes:
          - "yucca_data:/opt/yucca/data"
          - "yucca_ffmpeg:/opt/yucca/ffmpeg"
        networks:
          - yucca_network
        ports:
          - 9910:9910 # Web UI
          - 9911:9911 # Pprof
          - 9912:9912 # Telemetry
          - 1025:1025 # SMTP server
    EOF
    ```

Run Yucca:

```sh
docker compose up -d
```

After launch the Web interface will be available at http://ip-your-server:9910

## Additional

* [System requirements](/en/usage/SystemRequirements/)
* [Configuration](/en/configuration/)
