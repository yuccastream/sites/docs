---
title: What's new in Yucca 0.1.0
description: Video surveillance for home and business
---

# What's new in Yucca 0.1.0

*13.04.2020*

The translation is in progress.
The documentation is an open project, you can help with the translation, just open **Merge Request** 💻.
Try to read the [Russian version](/ru/release_notes/0.1.0/) using an [online translator](https://translate.yandex.ru/).

## What's next?

Installation guides may be found on [the Install page](/en/install/).
