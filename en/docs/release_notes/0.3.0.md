---
title: What's new in Yucca 0.3.0
description: Video surveillance for home and business
---

# What's new in Yucca 0.3.0

*10.08.2020*

The translation is in progress.
The documentation is an open project, you can help with the translation, just open **Merge Request** 💻.
Try to read the [Russian version](/ru/release_notes/0.3.0/) using an [online translator](https://translate.yandex.ru/).

## What's next?

Installation guides may be found on [the Install page](/en/install/).
