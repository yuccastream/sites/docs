---
title: Technical support
description: Video surveillance for home and business
---

# Technical support

Depending on the product, we provide different levels of technical support.  

!!! warning "Important"
     Support is provided on **business days from 9:00 to 17:00 [Europe/Belgade](https://www.timeanddate.com/time/zone/serbia/belgrade)**.  

## Free

Users of the Free version can ask for help from the community in our [Telegram chat](https://t.me/yuccastream).

!!! info "Terms"
     - Replies to emails are not guaranteed.
     - We provide consultations and answer questions in free form.
     - We do not provide remote diagnostics or assistance in installing or configuring the product (except if you find a bug and want to report it).

## Plus

Customers with a **Yucca Plus** subscription can ask for help from the community in our [Telegram chat](https://t.me/yuccastream).

!!! info "Terms"
     - Replies to emails are not guaranteed.
     - We provide consultations and answer questions in free form.
     - We do not provide remote diagnostics or assistance in installing or configuring the product (except if you find a bug and want to report it).

## Enterprise

Customers with a **Yucca Enterprise** subscription can reach out to the community for help in our [Telegram chat](https://t.me/yuccastream) or by mail <support@yucca.app>.

!!! info "Terms"
     - The response time for the 1st appeal is **1** business day.
     - We provide consultations and answer questions in free form.
     - You can also count on remote diagnosis of problems or assistance in installing and configuring the product and/or related components.

!!! question "What services are included in the list?"

     - Installing and updating Yucca on Linux-based OS
     - Troubleshooting Yucca
     - Installation and configuration of Standalone PostgreSQL for Yucca
     - Installation and configuration of Reverse Proxy server (Nginx, Traefik, HAProxy)
