---
title: Perfomance tests on Raspberry Pi 4
description: Video surveillance for home and business
---

# High utilization CPU when yucca idle

!!! info ""
    If you need expert help with this or any other issue, please see our terms and conditions **[technical support](/en/support/)**

## Description

In release 0.5.0, when installing from the [deb](/en/Install/Debian_Ubuntu.md/#_1) package, the default configuration is set to the wrong value for the [`stats_collection_interval`](/en/configuration/old/0.5.0/) parameter *0s* instead of *1s*.
This error can cause high CPU utilization by the yucca server when there is no real load.
Issue in tracker [#742](https://gitlab.com/yuccastream/yucca/-/issues/742)

![bug_742](/en/media/other/bug_742.png)

## Solution

Set a value from *1s* and higher:

```bash
[server]
stats_collection_interval = "1s"
```

The problem should be fixed.

In versions older than 0.5.0, the default is already the correct value, and should not appear.
