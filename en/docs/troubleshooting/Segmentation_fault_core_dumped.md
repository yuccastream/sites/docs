---
title: Segmentation fault (core dumped)
description: Video surveillance for home and business
---

# Segmentation fault (core dumped)

!!! info ""
    If you need expert help with this or any other issue, please see our terms and conditions **[technical support](/en/support/)**

## Description

This error may occur when trying to add a stream

[![segmentation-fault-core-dumped](/en/media/troubleshooting/Segmentation fault (core dumped).png)](/en/media/troubleshooting/Segmentation fault (core dumped).png)

you can also see it in the server log

```sh
WARN    latest/runner.go:308    Failed to create preview: signal: segmentation fault (core dumped)
```

## Solution

To fix the problem, you need to install the **nscd** package

in ubuntu/debian:

```sh
sudo apt install nscd
```

in fedora/centos:

```sh
dnf install nscd
```

## Reason

For Linux, we use a static ffmpeg build from <https://www.johnvansickle.com/ffmpeg>.
In readme.txt there is a note to the assembly:

```txt
Notes:  A limitation of statically linking glibc is the loss of DNS resolution. Installing
        nscd through your package manager will fix this.
```
