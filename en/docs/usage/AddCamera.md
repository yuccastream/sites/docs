---
title: Adding a Camera
description: Video surveillance for home and business
---

# Adding a Camera

!!! info "Only video in **h264** codec is supported"

On the homepage, click on the ➕ symbol in the top left corner, next to the logo.

[![add_0](/en/media/other/add_0.png)](/en/media/other/add_0.png)

In the input field, specify the link to the camera's RTSP stream. The exact format can be found in the camera's manual or using [ONVIF Device Manager](https://sourceforge.net/projects/onvifdm/) on Windows platforms.

HTTP (HLS/DASH) and RTMP streams can also be specified.

!!! note "Example link"

    ```bash
    rtsp://192.168.1.10:554/user=admin&password=&channel=1&stream=0.sdp?
    ```

Click **Check**.

[![add_1](/en/media/other/add_1.png)](/en/media/other/add_1.png)

If the stream is available, you will see a preview image and video/audio parameters.
Then fill in the name field; description and tags fields are optional.

[![add_2](/en/media/other/add_2.png)](/en/media/other/add_2.png)

Below, you can enable archive and specify the retention period.

!!! warning "Note that video archive takes up a lot of space on the hard drive"
    Yucca does not monitor free space; you need to do this yourself.
    The function of automatic purging and overwriting of the archive based on disk capacity will be available in the future.

![add_3](/en/media/other/add_3.png)
