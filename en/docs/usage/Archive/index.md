---
title: Archive
description: Video surveillance for home and business
---

# Archive

Basic functionality of all versions, its operation in the paid version does not differ from the free version.
Simple and intuitive interface for navigation does not require special instructions to start using.

## Overview

When setting up a stream, the recording depth is defined in hours, and the current hour is not taken into account when rotating.
Archive recording on receipt of an event is also available, but requires [additional settings](/en/usage/smtp/) on the camera side.

[![settings](/en/media/features/Archive/settings.png)](/en/media/features/Archive/settings.png)

In the navigation, you can see available(green) and unavailable(red) areas of the archive and *not yet*(gray) .

[![bar](/en/media/features/Archive/bar.png)](/en/media/features/Archive/bar.png)

You can use the buttons in the bar or the mouse wheel for control.

## Example of work

<video loop autoplay muted>
  <source src="/en/media/features/Archive/Archive.mp4" type="video/mp4">
</video>
