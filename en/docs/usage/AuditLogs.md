---
title: AuditLogs
description: Video surveillance for home and business
---

!!! info "💰 Function available with [**subscription**](https://yucca.app/en/prices) to Yucca Enterprise"

# Audit Logs

## Overview

The function allows viewing user actions and can be useful for information security specialists.

[![AuditLogs1](/en/media/features/AuditLogs/AuditLogs1.png)](/en/media/features/AuditLogs/AuditLogs1.png)

## Configuration

To learn how to configure audit logs, refer to the section on [configuration](/en/configuration/#audit_logs).
