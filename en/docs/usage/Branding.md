---
title: Branding
description: Video surveillance for home and business
---

# Branding

!!! info "💰 Feature available by [**subscription**](https://yucca.app/en/prices) Yucca Enterprise"

## Overview

The branding feature in Yucca in allows you to replace the logo, favicon, header, documentation and website links with your own.  

## Configuration

To learn how to configure branding please refer to the section in [configuration](/en/configuration/#branding)

## Branding Examples

[![Photoshop1](/en/media/features/Branding/Photoshop1.png)](/en/media/features/Branding/Photoshop1.png)
[![Photoshop2](/en/media/features/Branding/Photoshop2.png)](/en/media/features/Branding/Photoshop2.png)
[![google1](/en/media/features/Branding/google1.png)](/en/media/features/Branding/google1.png)
[![google2](/en/media/features/Branding/google2.png)](/en/media/features/Branding/google2.png)
[![hetzner1](/en/media/features/Branding/hetzner1.png)](/en/media/features/Branding/hetzner1.png)
[![hetzner2](/en/media/features/Branding/hetzner2.png)](/en/media/features/Branding/hetzner2.png)

Translated with DeepL.com (free version)