---
title: Working with Cluster
description: Video surveillance for home and business
---

# Working with Cluster

!!! info "💰 Function available with [**subscription**](https://yucca.app/en/prices) to Yucca Enterprise"

## Adding Cameras

In cluster mode, when adding a camera, an additional server selection field appears.
If no server is specified, the camera will automatically be placed on the server with the fewest streams.

[![addStream](/en/media/features/Cluster/addStream.png)](/en/media/features/Cluster/addStream.png)

## Maintenance

If you need to perform any work and temporarily take a server out of service, you can **drain** it.

[![drain](/en/media/features/Cluster/drain.png)](/en/media/features/Cluster/drain.png)

After draining, the other nodes in the cluster will consider it unavailable, as well as the cameras from that server.
Users will see a corresponding *placeholder*, indicating that the stream is unavailable because the server with the stream is unavailable.
To return the node to the cluster, you need to restart the Yucca process.

## Removal

You can only remove a node from the cluster if it has no streams.
