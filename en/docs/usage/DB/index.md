---
title: Database
description: Video surveillance for home and business
---

# Database

## Overview

Yucca requires a database to operate. By default, this is Sqlite, and the database file is stored in the [state_dir](/en/configuration/#path) directory. Sqlite is sufficient for working with a small number of cameras and motion events, plus no additional configuration is required. However, for production environments with a large number of cameras (more than 10) and motion detection enabled, we recommend using [PostgreSQL](/en/usage/DB/postgres/) as a more performant database.

## Next Steps

- [Installing PostgreSQL](/en/usage/DB/postgres/)
- [Sqlite to PostgreSQL Migration](/en/usage/DB/migration/)
