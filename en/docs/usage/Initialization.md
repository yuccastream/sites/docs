---
title: Server initialization
description: Video surveillance for home and business
---

# Server initialization

When you start the server for the first time, you need to create a system administrator. This user cannot be deleted.

## WEB interface

Open the link in the browser where the server is available.
Enter the details:

- login
- e-mail
- password
- user’s interface language

Then press **Create**.

[![init](/en/media/other/en_init.png)](/en/media/other/en_init.png)

## CLI

```bash
/opt/yucca/yucca admin unlock --name admin --email admin@example.com --password admin1234
```
