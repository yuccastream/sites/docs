---
title: Mosaic
description: Video surveillance for home and business
---

# Mosaic

Basic functionality of all versions, its operation in the paid version does not differ from the free version.

## Overview

The feature allows you to watch live stream from multiple cameras at the same time.
When creating a mosaic, add the required cameras and save it.
You can also make the mosaic available to all users on the server.

[![mosaic](/en/media/features/Mosaic/mosaic.png)](/en/media/features/Mosaic/mosaic.png)

## Example of work

<video loop autoplay muted>
  <source src="/en/media/features/Mosaic/Mosaic.mp4" type="video/mp4">
</video>
