---
title: Public streams
description: Video surveillance for home and business
---

# Public streams

!!! info "💰 Feature available with [**subscription**](https://yucca.app/en/prices) Yucca Plus or Yucca Enterprise""

## Overview

Allows you to embed a camera broadcast to your website or third-party player.

<video loop autoplay muted>
  <source src="/en/media/features/StreamShare/StreamShare1.mp4" type="video/mp4">
</video>
