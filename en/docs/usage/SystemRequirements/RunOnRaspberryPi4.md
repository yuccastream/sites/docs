---
title: Perfomance tests on Raspberry Pi 4
description: Video surveillance for home and business
---

# Perfomance tests on Raspberry Pi 4

*29.12.2020.*

[![raspberry-pi-4-label](/en/media/note/run_on_rpi4/raspberry-pi-4-label.png)](/en/media/note/run_on_rpi4/raspberry-pi-4-label.png)

Recently, Yucca added support for ARM and ARM64. Then we did a test – how many cameras can be added  to the popular Raspberry Pi single board computer.

## Testing

We used the youngest Raspberry Pi 4 board with 2 GB RAM and the [Geekworm x832](https://geekworm.com/products/for-raspberry-pi-4-x832-3-5-inch-sata-hdd-storage-expansion-board) board for connecting 3,5' HDD. A video review is available on our [Youtube channel](https://www.youtube.com/watch?v=bF1ogyj0NvU).

Installation process on a Raspberry Pi and on a home Linux PC is same. You just need to choose the correct architecture in the [documentation section](/en//Install/Debian_Ubuntu/). For  the Raspberry Pi 3 and 4, the ARM architecture is suitable, and for the Raspberry 4 with 8 GB RAM , choose ARM64.
After assembling and installing Yucca, we added 2 cameras with archive recording on HDD. We  the following indicators during the day:

After assembling and installing Yucca, we added 2 cameras with archive recording on HDD. We  the following indicators during the day:
[![01](/en/media/note/run_on_rpi4/01.png)](/en/media/note/run_on_rpi4/01.png)

Hard Drive metrics (HDD):
[![02](/en/media/note/run_on_rpi4/02.png)](/en/media/note/run_on_rpi4/02.png)

In terms of performance, working on a Raspberry Pi is not much different from working with AMD64 in a data centre:
<!-- [![Raspberry Pi 4 Video](https://img.youtube.com/vi/d2gPGHzPUFk/0.jpg)](https://www.youtube.com/watch?v=d2gPGHzPUFk) -->
<iframe width="960" height="480" src="https://www.youtube.com/embed/d2gPGHzPUFk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Let’s try to squeeze the maximum out of the board – add 10 more streams. General indicators:
[![03](/en/media/note/run_on_rpi4/03.png)](/en/media/note/run_on_rpi4/03.png)

Hard Drive metrics (HDD):
[![04](/en/media/note/run_on_rpi4/04.png)](/en/media/note/run_on_rpi4/04.png)

LA has grown a lot, but this is normal since 2 processes are launched for each stream. Many cameras-many processes-many interrups. When LA goes over 1, a queue will form and this will affect responsiveness, but will continue to work. In total, I was able to add 21 cameras:
[![05](/en/media/note/run_on_rpi4/05.png)](/en/media/note/run_on_rpi4/05.png)

The archive is working, but LIVE was no longer possible to watch. What in the charts?  Everything is bad there – the processor has reached the limit – LA is more than 5. General indicators: 
[![06](/en/media/note/run_on_rpi4/06.png)](/en/media/note/run_on_rpi4/06.png)

Hard Drive metrics (HDD):
[![07](/en/media/note/run_on_rpi4/07.png)](/en/media/note/run_on_rpi4/07.png)

That’s all. We don’t recommend to add more than 10-12 cameras on the Yucca, because this will affect the performance and responsiveness of the software, but that result is an excellent for a 35$ single board computer. We are already working on a improved architecture to reduce the workload. As a result – add more cameras.
[Enterprise version](https://yucca.app/en/#rate) of Yucca can be using by a small internet providers, CCTV integrators, small business, developers and many more. About advanced tools and terms read on our [web-site](https://yucca.app/). Ask your questions for us using feedback form, e-mail [info@yucca.app](mailto:info@yucca.app) or [Telegram-chat](https://t.me/yuccastream).
