---
title: Configuring Motion Detection by email on Dahua camera
description: Video surveillance for home and business
---

# Configuring Motion Detection by email on Dahua camera

Open the web-interface of the camera with administrator rights.
Go to **Setting > Network > SMTP (Email)**

[![dahua_01](/en/media/other/smtp/dahua_01.png)](/en/media/other/smtp/dahua_01.png)

---

Enter connection details - *SMTP server*, *Port*, *Sender* and *Mail Receiver*. The last 2 must match. If authorization is enabled in SMTP server settings, enter *username* and *password*.

Press **Test**. If everything is correct, you’ll see a message `Succeed` and in the logs of Yucca message about created event:

```log
Event annotation created, stream_id: 10
```

And a yellow marker will appear in the camera’s progress bar.

[![dahua_02](/en/media/other/smtp/dahua_02.png)](/en/media/other/smtp/dahua_02.png)
[![bar_event](/en/media/other/smtp/bar_event.png)](/en/media/other/smtp/bar_event.png)

---

Next, you need to enable motion detection. Go to **Setting > Event > Video Detection > Motion Detection** and turn on *enable* option and *Send Email* as a type of notification.

[![dahua_03](/en/media/other/smtp/dahua_03.png)](/en/media/other/smtp/dahua_03.png)

---

In the *Area* field by clicking *Setting* button, you can set visibility area and sensitivity level of Motion Detection.

[![dahua_04](/en/media/other/smtp/dahua_04.png)](/en/media/other/smtp/dahua_04.png)
