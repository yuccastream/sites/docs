site_name: Yucca Documetation
site_url: https://docs.yucca.app/en/
copyright: Copyright &copy; <a href="https://yucca.app/en"  target="_blank" rel="noopener">Yucca Stream</a>, 2024
theme:
  name: material
  palette:
    primary: blue grey
    accent: red
  custom_dir: overrides
  language: en
  favicon: media/favicon.ico
  features:
    - navigation.instant
    - content.tabs.link
    - content.code.copy
plugins:
  - social:
      cards_layout_options:
        background_color: "#4b4d64"
  - search:
      lang:
        - en
        - ru
  - redirects:
      redirect_maps:
        "install/CentOS_RHEL_Fedora_SUSE.md": "install/RHEL_CentOS.md"
        "install/Synology_NAS.md": "install/other_linux.md"

markdown_extensions:
  - abbr
  - admonition
  - attr_list
  - def_list
  - footnotes
  - md_in_html
  - tables
  - pymdownx.betterem
  - pymdownx.caret
  - pymdownx.critic
  - pymdownx.details
  - pymdownx.highlight
  - pymdownx.inlinehilite
  - pymdownx.snippets
  - pymdownx.superfences
  - pymdownx.tilde
  - pymdownx.tabbed:
      alternate_style: true
  # # [:octicons-tag-24: 0.7.0](/ru/release_notes/0.7.0/)
  - pymdownx.emoji:
      emoji_index: !!python/name:materialx.emoji.twemoji
      emoji_generator:
        !!python/name:materialx.emoji.to_svg # <mark>deprecated</mark>

  - pymdownx.mark
  # `#!py3 import pymdownx; pymdownx.__version__`
  - pymdownx.inlinehilite
  # ++ctrl+alt+delete++
  - pymdownx.keys
  # # (tm), -->
  # - pymdownx.smartsymbols
  # # - [X] item 1
  # #     * [X] item A
  # # - [ ] item 2
  # - pymdownx.tasklist:
  #     custom_checkbox: true
  - toc:
      permalink: true
      separator: "_"

extra:
  alternate:
    - name: English
      link: /en/
      lang: en
    - name: Русский
      link: /ru/
      lang: ru
  social:
    - icon: fontawesome/solid/envelopes-bulk
      link: mailto:info@yucca.app
    - icon: fontawesome/brands/telegram
      link: https://t.me/yuccastream
    - icon: fontawesome/brands/github
      link: https://github.com/yuccastream
    - icon: fontawesome/brands/gitlab
      link: https://gitlab.com/yuccastream

nav:
  - index.md
  - Release notes:
      - "0.10.2": release_notes/0.10.2.md
      - "0.10.1": release_notes/0.10.1.md
      - "0.10.0": release_notes/0.10.0.md
      - "Old":
          - "0.9.6": release_notes/0.9.6.md
          - "0.9.5": release_notes/0.9.5.md
          - "0.9.4": release_notes/0.9.4.md
          - "0.9.3": release_notes/0.9.3.md
          - "0.9.2": release_notes/0.9.2.md
          - "0.9.1": release_notes/0.9.1.md
          - "0.9.0": release_notes/0.9.0.md
          - "0.8.0": release_notes/0.8.0.md
          - "0.7.4": release_notes/0.7.4.md
          - "0.7.3": release_notes/0.7.3.md
          - "0.7.2": release_notes/0.7.2.md
          - "0.7.1": release_notes/0.7.1.md
          - "0.7.0": release_notes/0.7.0.md
          - "0.6.0": release_notes/0.6.0.md
          - "0.5.1": release_notes/0.5.1.md
          - "0.5.0": release_notes/0.5.0.md
          - "0.4.0": release_notes/0.4.0.md
          - "0.3.1": release_notes/0.3.1.md
          - "0.3.0": release_notes/0.3.0.md
          - "0.2.0": release_notes/0.2.0.md
          - "0.1.1": release_notes/0.1.1.md
          - "0.1.0": release_notes/0.1.0.md
  - Install:
      - "Overview": install/index.md
      - "Docker": install/Docker.md
      - "Debian/Ubuntu": install/Debian_Ubuntu.md
      - "SUSE": install/SUSE.md
      - "RHEL/CentOS": install/RHEL_CentOS.md
      - "Other Linux": install/other_linux.md
      - "macOS": install/macOS.md
  - Configuration:
      - Latest: configuration/index.md
      - Old versions:
          - "0.9.0": configuration/old/0.9.0.md
          - "0.8.0": configuration/old/0.8.0.md
          - "0.7.0": configuration/old/0.7.0.md
          - "0.6.0": configuration/old/0.6.0.md
          - "0.5.1": configuration/old/0.5.1.md
          - "0.5.0": configuration/old/0.5.0.md
          - "0.4.0": configuration/old/0.4.0.md
          - "0.3.1": configuration/old/0.3.1.md
  - Guides:
      - "System requirements":
          - "Overview": usage/SystemRequirements/index.md
          - "Hardware selection": usage/SystemRequirements/HardwareSelection.md
          - "Performance testing on Raspberry Pi 4": usage/SystemRequirements/RunOnRaspberryPi4.md
      - "Database":
          - "Overview": usage/DB/index.md
          - "Install Postgres": usage/DB/postgres.md
          - "Migration Sqlite to Postgres": usage/DB/migration.md
      - "Initialization": usage/Initialization.md
      - "Add camera": usage/AddCamera.md
      - "Archive": 
        - "Overview": usage/Archive/index.md
        - "Changing path for storing the archive": usage/Archive/ChangeAllocDir.md
      - "Mosaic": usage/Mosaic.md
      - "Reset password": usage/ResetPassword.md
      - "License": usage/SetupLicense.md
      - "Users": usage/Users.md
      - "Quota": usage/Quota.md
      - "Stream ACL": usage/StreamACL.md
      - "Branding": usage/Branding.md
      - "Tunnel": usage/Tunnel.md
      - "Cluster":
          - "Overview": usage/Cluster/index.md
          - "Setup": usage/Cluster/setup.md
          - "Maintenance": usage/Cluster/maintenance.md
      - "AuditLogs": usage/AuditLogs.md
      - "Stream share": usage/StreamShare.md
      - "Gateway":
          - "Overview": usage/Gateway/index.md
          - "Setup": usage/Gateway/setup.md
      - "Motion Detection(Email)":
          - "Overview": usage/smtp/index.md
          - "Dahua cameras": usage/smtp/dahua.md
          - "Hikvision / HiWatch cameras": usage/smtp/hikvision_hiwatch.md
          - "XM cameras": usage/smtp/xm.md
      - "Nginx + Let's Encrypt": usage/Nginx.md
      - "API tokens": usage/PersonalTokens.md
  #  - "Команды": usage/Teams.md
  #  - "Уведомления": usage/Notifications.md
  - Troubleshooting:
      - troubleshooting/inotify_configuration.md
      - troubleshooting/High_cpu_load.md
      - troubleshooting/Segmentation_fault_core_dumped.md
  - Technical support: support.md
