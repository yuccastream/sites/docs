---
title: Конфигурация
description: Видеонаблюдение для дома и бизнеса
---

# Конфигурация

Настройка может осуществляться через конфигурационный файл, аргументы командной строки или переменные окружения. Приоритет при чтении конфигурации:

1. файл;
1. переменные окружения;
1. аргументы командной строки.

Параметры делятся на логические разделы (блоки), которые соответствуют префиксу в аргументах командной строки и переменных окружения.

## server

Глобальные параметры сервера.

### log_level

Уровень логирования для сервера. Возможные значения: `panic`, `fatal`, `error`, `warning`, `info`, `debug`).

!!! note "По умолчанию `info`"
    Значение можно определить одним из способов:

    * с помощью аргумента запуска: `--log-level=info`
    * с помощью переменной окружения: `#!shell export YUCCA_SERVER_LOG_LEVEL=info`
    * с помощью конфигурационного файла:

    ```toml
    # /opt/yucca/yucca.toml
    [server]
    log_level = "info"
    ```

### data_dir

Глобальный каталог для хранения состояния (в случае использования sqlite3) и данных потоков.

!!! note "По умолчанию `./data`"
    Значение можно определить одним из способов:

    * с помощью аргумента запуска: `--data-dir=/opt/yucca/data/`
    * с помощью переменной окружения: `#!shell export YUCCA_SERVER_DATA_DIR=/opt/yucca/data/`
    * с помощью конфигурационного файла:

    ```toml
    # /opt/yucca/yucca.toml
    [server]
    data_dir = "/opt/yucca/data/"
    ```

### alloc_dir

Каталог с данными потоков (видео архив и прочее).

!!! note "По умолчанию `<data_dir>/alloc`"
    Значение можно определить одним из способов:

    * с помощью аргумента запуска: `--alloc-dir=/opt/yucca/data/alloc/`
    * с помощью переменной окружения: `#!shell export YUCCA_SERVER_ALLOC_DIR=/opt/yucca/data/alloc/`
    * с помощью конфигурационного файла:

    ```toml
    # /opt/yucca/yucca.toml
    [server]
    alloc_dir = "/opt/yucca/data/alloc/"
    ```

### state_dir

Каталог с файлом базы данных (в случае использования sqlite3).

!!! note "По умолчанию `<data_dir>/server`"
    Значение можно определить одним из способов:

    * с помощью аргумента запуска: `--state-dir=/opt/yucca/data/server/`
    * с помощью переменной окружения: `#!shell export YUCCA_SERVER_STATE_DIR=/opt/yucca/data/server/`
    * с помощью конфигурационного файла:

    ```toml
    # /opt/yucca/yucca.toml
    [server]
    state_dir = "/opt/yucca/data/server/"
    ```

### temp_dir

Каталог tmpfs для данных с коротким сроком хранения.
*(тут хранятся ротируемые для живого потока чанки)*

!!! note "По умолчанию `/dev/shm/yucca`"
    Значение можно определить одним из способов:

    * с помощью аргумента запуска: `--temp-dir=/dev/shm/yucca`
    * с помощью переменной окружения: `#!shell export YUCCA_SERVER_TEMP_DIR=/dev/shm/yucca`
    * с помощью конфигурационного файла:

    ```toml
    # /opt/yucca/yucca.toml
    [server]
    temp_dir = "/dev/shm/yucca"
    ```
### alloc_dir_warning_threshold

Пороговый процент для предупреждения об использовании диска.

!!! note "По умолчанию `80`"
    Значение можно определить одним из способов:

    * с помощью аргумента запуска: `--alloc-dir-warning-threshold=80`
    * с помощью переменной окружения: `#!shell export YUCCA_SERVER_ALLOC_DIR_WARNING_THRESHOLD=80`
    * с помощью конфигурационного файла:

    ```toml
    # /opt/yucca/yucca.toml
    [server]
    alloc_dir_warning_threshold = "80"
    ```

### stats_collection_interval

Интервал сборка статистики по ресурсам системы.

!!! note "По умолчанию `1s`"
    Значение можно определить одним из способов:

    * с помощью аргумента запуска: `--stats-collection-interval=1s`
    * с помощью переменной окружения: `#!shell export YUCCA_SERVER_STATS_COLLECTION_INTERVAL=1s`
    * с помощью конфигурационного файла:

    ```toml
    # /opt/yucca/yucca.toml
    [server]
    stats_collection_interval = "1s"
    ```

### default_language

Язык интерфейса по умолчанию у новых пользователей. Возможные значения: `en`, `ru`, `uk`.

!!! note "По умолчанию `en`"
    Значение можно определить одним из способов:

    * с помощью аргумента запуска: `--default-language=en`
    * с помощью переменной окружения: `#!shell export YUCCA_SERVER_DEFAULT_LANGUAGE=en`
    * с помощью конфигурационного файла:

    ```toml
    # /opt/yucca/yucca.toml
    [server]
    default_language = "en"
    ```

### domain

Используется для определения поля *Server* при настройке [получения событий по Email](/ru/usage/smtp/)

!!! note "По умолчанию не определено `""`"
    Значение можно определить одним из способов:

    * с помощью аргумента запуска: `--domain=demo.yucca.app`
    * с помощью переменной окружения: `#!shell export YUCCA_SERVER_DOMAIN=192.168.188.15`
    * с помощью конфигурационного файла:

    ```toml
    # /opt/yucca/yucca.toml
    [server]
    domain = "demo.yucca.app"
    ```

### health_check_interval

Интервал для проверки работоспособности зависимостей.

!!! note "По умолчанию `"30s"`"
    Значение можно определить одним из способов:

    * с помощью аргумента запуска: `--health-check-interval="30s"`
    * с помощью переменной окружения: `#!shell export YUCCA_SERVER_HEALTH_CHECK_INTERVAL="30s"`
    * с помощью конфигурационного файла:

    ```toml
    # /opt/yucca/yucca.toml
    [server]
    health_check_interval = "30s"
    ```

### listen_address

IP адрес и порт на котором будет доступен веб интерфейс.

!!! note "По умолчанию `:9910`"
    Значение можно определить одним из способов:

    * с помощью аргумента запуска: `--web.listen-address=:9910`
    * с помощью переменной окружения: `#!shell export YUCCA_SERVER_LISTEN_ADDRESS=:9910`
    * с помощью конфигурационного файла:

    ```toml
    # /opt/yucca/yucca.toml
    [server]
    listen_address = ":9910"
    ```

### log_file

Путь к файлу с логами, если не указано, то stdout.

!!! note "По умолчанию не определено `""`"
    Значение можно определить одним из способов:

    * с помощью аргумента запуска: `--log-file=/var/log/yucca.log`
    * с помощью переменной окружения: `#!shell export YUCCA_SERVER_LOG_FILE=/var/log/yucca.log`
    * с помощью конфигурационного файла:

    ```toml
    # /opt/yucca/yucca.toml
    [server]
    log_file = "/var/log/yucca.log"
    ```

### self_logs

Просмотр логов сервера и потоков в интерфейсе во вкладке **отладка**.

!!! note "По умолчанию `true`"
    Значение можно определить одним из способов:

    * с помощью аргумента запуска: `--self-logs=true`
    * с помощью переменной окружения: `#!shell export YUCCA_SERVER_SELF_LOGS=false`
    * с помощью конфигурационного файла:

    ```toml
    # /opt/yucca/yucca.toml
    [server]
    self_logs = true
    ```

### log_buffer_size

Колличество последних сообщений в интерфейсе во вкладке **отладка**.

!!! note "По умолчанию `50`"
    Значение можно определить одним из способов:

    * с помощью аргумента запуска: `--log-buffer-size=50`
    * с помощью переменной окружения: `#!shell export YUCCA_SERVER_LOG_BUFFER_SIZE=50`
    * с помощью конфигурационного файла:

    ```toml
    # /opt/yucca/yucca.toml
    [server]
    log_buffer_size = 50
    ```

### cert_file

Путь к файлу сертификата, в случае использования HTTPS.

!!! note "По умолчанию не определено `""`"
    Значение можно определить одним из способов:

    * с помощью аргумента запуска: `--cert-file=/opt/yucca/ssl/cert.crt`
    * с помощью переменной окружения: `#!shell export YUCCA_SERVER_CERT_FILE=/opt/yucca/ssl/cert.crt`
    * с помощью конфигурационного файла:

    ```toml
    # /opt/yucca/yucca.toml
    [server]
    cert_file = "/opt/yucca/ssl/cert.crt"
    ```

### cert_key

Путь к файлу ключа, в случае использования HTTPS.

!!! note "По умолчанию не определено `""`"
    Значение можно определить одним из способов:

    * с помощью аргумента запуска: `--cert-key=/opt/yucca/ssl/cert.key`
    * с помощью переменной окружения: `#!shell export YUCCA_SERVER_CERT_KEY=/opt/yucca/ssl/cert.key`
    * с помощью конфигурационного файла:

    ```toml
    # /opt/yucca/yucca.toml
    [server]
    cert_key = "/opt/yucca/ssl/cert.key"
    ```

### pprof

Включить профилирование.

!!! note "По умолчанию `false`"
    Значение можно определить одним из способов:

    * с помощью аргумента запуска: `--pprof=true`
    * с помощью переменной окружения: `#!shell export YUCCA_SERVER_PPROF=true`
    * с помощью конфигурационного файла:

    ```toml
    # /opt/yucca/yucca.toml
    [server]
    pprof = true
    ```

### pprof_listen_address

IP адрес и порт на котором будет доступен pprof профилировщик.

!!! note "По умолчанию `:9911`"
    Значение можно определить одним из способов:

    * с помощью аргумента запуска: `--pprof-listen-address=":9911"`
    * с помощью переменной окружения: `#!shell export YUCCA_SERVER_PPROF_LISTEN_ADDRESS=":9911"`
    * с помощью конфигурационного файла:

    ```toml
    # /opt/yucca/yucca.toml
    [server]
    pprof_listen_address = ":9911"
    ```

### validate

Флаг для проверки корректности конфигурации.

!!! note ""

    Значение можно определить одним из способов:

    * с помощью аргумента запуска: `--validate`

    Пример использования

    ```sh
    ./yucca server --config /opt/yucca/yucca.toml --validate
    Valid configuration
    ```

## database

Подключение к базе данных.

### type

Поддерживается 2 типа баз данных для хранения состояния – `sqlite3` и `postgres`. В небольших инсталляциях, достаточно `sqlite3`, при большом количестве потоков, мы рекомендуем использовать `postgres`.

!!! note "По умолчанию `sqlite3`"
    Значение можно определить одним из способов:

    * с помощью аргумента запуска: `--database-type=sqlite3`
    * с помощью переменной окружения: `#!shell export YUCCA_DATABASE_TYPE=sqlite3`
    * с помощью конфигурационного файла:

    ```toml
    # /opt/yucca/yucca.toml
    [database]
    type = "sqlite3"
    ```

### path

Путь к файлу базы (только для sqlite3).

!!! note "По умолчанию `<state_dir>/state.db`"
    Значение можно определить одним из способов:

    * с помощью аргумента запуска: `--database-path=/opt/yucca/server/state.db`
    * с помощью переменной окружения: `#!shell export YUCCA_DATABASE_PATH=/opt/yucca/server/state.db`
    * с помощью конфигурационного файла:

    ```toml
    # /opt/yucca/yucca.toml
    [database]
    path = "/opt/yucca/server/state.db"
    ```

### host

Адрес подключения к базе (только для postgres).

!!! note "По умолчанию `127.0.0.1:5432`"
    Значение можно определить одним из способов:

    * с помощью аргумента запуска: `--database-host=127.0.0.1:5432`
    * с помощью переменной окружения: `#!shell export YUCCA_DATABASE_HOST=127.0.0.1:5432`
    * с помощью конфигурационного файла:

    ```toml
    # /opt/yucca/yucca.toml
    [database]
    host = "127.0.0.1:5432"
    ```

### name

Название базы данных (только для postgres).

!!! note "По умолчанию `yucca`"
    Значение можно определить одним из способов:

    * с помощью аргумента запуска: `--database-name=yucca`
    * с помощью переменной окружения: `#!shell export YUCCA_DATABASE_NAME=yucca`
    * с помощью конфигурационного файла:

    ```toml
    # /opt/yucca/yucca.toml
    [database]
    name = "yucca"
    ```

### user

Имя пользователя для подключения (только для postgres).

!!! note "По умолчанию `postgres`"
    Значение можно определить одним из способов:

    * с помощью аргумента запуска: `--database-user=postgres`
    * с помощью переменной окружения: `#!shell export YUCCA_DATABASE_USER=postgres`
    * с помощью конфигурационного файла:

    ```toml
    # /opt/yucca/yucca.toml
    [database]
    user = "postgres"
    ```

### password

Пароль пользователя для подключения (только для postgres).

!!! note "По умолчанию `postgres`"
    Значение можно определить одним из способов:

    * с помощью аргумента запуска: `--database-password=postgres`
    * с помощью переменной окружения: `#!shell export YUCCA_DATABASE_PASSWORD=postgres`
    * с помощью конфигурационного файла:

    ```toml
    # /opt/yucca/yucca.toml
    [database]
    password = "postgres"
    ```

### ssl_mode

Использовать при подключении SSL (шифрованное соединение) (только для postgres). Возможные значения: `disable`, `require`, `verify-full`).

!!! note "По умолчанию `disable`"
    Значение можно определить одним из способов:

    * с помощью аргумента запуска: `--database-ssl-mode=disable`
    * с помощью переменной окружения: `#!shell export YUCCA_DATABASE_SSL_MODE=disable`
    * с помощью конфигурационного файла:

    ```toml
    # /opt/yucca/yucca.toml
    [database]
    ssl_mode = "disable"
    ```

### busy_timeout

Тайм-аут в секундах для ожидания разблокировки таблицы SQLite (только для sqlite3).

!!! note "По умолчанию `500`"
    Значение можно определить одним из способов:

    * с помощью аргумента запуска: `--database-busy-timeout=500`
    * с помощью переменной окружения: `#!shell export YUCCA_DATABASE_BUSY_TIMEOUT=500`
    * с помощью конфигурационного файла:

    ```toml
    # /opt/yucca/yucca.toml
    [database]
    busy_timeout = 500
    ```

### ca_cert_path

Путь к сертификату центра сертификации (CA) случае использования SSL (только для postgres).

!!! note "По умолчанию `/etc/ssl/certs`"
    Значение можно определить одним из способов:

    * с помощью аргумента запуска: `--database-ca-cert-path=/etc/ssl/certs`
    * с помощью переменной окружения: `#!shell export YUCCA_DATABASE_CA_CERT_PATH=/etc/ssl/certs`
    * с помощью конфигурационного файла:

    ```toml
    # /opt/yucca/yucca.toml
    [database]
    ca_cert_path = "/etc/ssl/certs"
    ```

### client_cert_path

Путь к файлу с сертификатом в случае использования SSL (только для postgres).

!!! note "По умолчанию не определено `""`"
    Значение можно определить одним из способов:

    * с помощью аргумента запуска: `--database-client-cert-path=/opt/yucca/ssl/cert.crt`
    * с помощью переменной окружения: `#!shell export YUCCA_DATABASE_CLIENT_CERT_PATH=/opt/yucca/ssl/cert.crt`
    * с помощью конфигурационного файла:

    ```toml
    # /opt/yucca/yucca.toml
    [database]
    client_cert_path = "/opt/yucca/ssl/cert.crt"
    ```

### client_key_path

Путь к файлу с ключом в случае использования SSL (только для postgres).

!!! note "По умолчанию не определено `""`"
    Значение можно определить одним из способов:

    * с помощью аргумента запуска: `--database-client-key-path=/opt/yucca/ssl/cert.key`
    * с помощью переменной окружения: `#!shell export YUCCA_DATABASE_CLIENT_KEY_PATH=/opt/yucca/ssl/cert.key`
    * с помощью конфигурационного файла:

    ```toml
    # /opt/yucca/yucca.toml
    [database]
    client_key_path = "/opt/yucca/ssl/cert.key"
    ```

### cache_mode

Параметр общего кэша, используемый для подключения к базе данных (только для sqlite3). Возможные значения: `private`, `shared`.

!!! note "По умолчанию `shared`"
    Значение можно определить одним из способов:

    * с помощью аргумента запуска: `--database-cache-mode=shared`
    * с помощью переменной окружения: `#!shell export YUCCA_DATABASE_CACHE_MODE=shared`
    * с помощью конфигурационного файла:

    ```toml
    # /opt/yucca/yucca.toml
    [database]
    cache_mode = "shared"
    ```

### conn_max_lifetime

Устанавливает максимальное время, в течение которого соединение может быть повторно использовано.

!!! note "По умолчанию `0s`"
    Значение можно определить одним из способов:

    * с помощью аргумента запуска: `--database-conn-max-lifetime=0s`
    * с помощью переменной окружения: `#!shell export YUCCA_DATABASE_CONN_MAX_LIFETIME=0s`
    * с помощью конфигурационного файла:

    ```toml
    # /opt/yucca/yucca.toml
    [database]
    conn_max_lifetime = "0s"
    ```

### log_queries

Логировать SQL-запросы и время выполнения.

!!! note "По умолчанию `false`"
    Значение можно определить одним из способов:

    * с помощью аргумента запуска: `--database-log-queries=false`
    * с помощью переменной окружения: `#!shell export YUCCA_DATABASE_LOG_QUERIES=false`
    * с помощью конфигурационного файла:

    ```toml
    # /opt/yucca/yucca.toml
    [database]
    log_queries = false
    ```

### max_open_conn

Максимальное количество открытых соединений с базой данных (только для postgres).

!!! note "По умолчанию `0`"
    Значение можно определить одним из способов:

    * с помощью аргумента запуска: `--database-max-open-conn=0`
    * с помощью переменной окружения: `#!shell export YUCCA_DATABASE_MAX_OPEN_CONN=0`
    * с помощью конфигурационного файла:

    ```toml
    # /opt/yucca/yucca.toml
    [database]
    max_open_conn = 0
    ```

### max_idle_conn

Максимальное количество соединений в ожидании (только для postgres).

!!! note "По умолчанию `2`"
    Значение можно определить одним из способов:

    * с помощью аргумента запуска: `--database-max-idle-conn=2`
    * с помощью переменной окружения: `#!shell export YUCCA_DATABASE_MAX_IDLE_CONN=2`
    * с помощью конфигурационного файла:

    ```toml
    # /opt/yucca/yucca.toml
    [database]
    max_idle_conn = 2
    ```

## executor

Параметры запуска потоков.

### type

Тип исполнителя для парковки потоков. Доступные значения `exec`.

- **Exec** - сервер запускает ffmpeg и следит за его работой, в случае проблем перезапускает.

!!! note "По умолчанию `exec`"
    Значение можно определить одним из способов:

    * с помощью аргумента запуска: `--executor=exec`
    * с помощью переменной окружения: `#!shell export YUCCA_EXECUTOR_TYPE=exec`
    * с помощью конфигурационного файла:

    ```toml
    # /opt/yucca/yucca.toml
    [executor]
    type = "exec"
    ```

### log_level

Уровень логирования executor. Возможные значения: `panic`, `fatal`, `error`, `warning`, `info`, `debug`).

!!! note "По умолчанию `info`"
    Значение можно определить одним из способов:

    * с помощью аргумента запуска: `--executor-log-level=info`
    * с помощью переменной окружения: `#!shell export YUCCA_EXECUTOR_LOG_LEVEL=info`
    * с помощью конфигурационного файла:

    ```toml
    # /opt/yucca/yucca.toml
    [executor]
    log_level = "info"
    ```

### exec

Дополнительные параметры для типа `exec`.

#### cancel_grace_period

Интервал ожидания завершения всех запущенных подпроцессов (ffmpeg, ffprobe, ...).

!!! note "По умолчанию `5s`"
    Значение можно определить одним из способов:

    * с помощью аргумента запуска: `--exec-cancel-grace-period=5s`
    * с помощью переменной окружения: `#!shell export YUCCA_EXECUTOR_EXEC_CANCEL_GRACE_PERIOD=5s`
    * с помощью конфигурационного файла:

    ```toml
    # /opt/yucca/yucca.toml
    [executor.exec]
    cancel_grace_period = "5s"
    ```

#### ffmpeg_log_level

Уровень логирования для процессов ffmpeg.

!!! note "По умолчанию `error`"
    Значение можно определить одним из способов:

    * с помощью аргумента запуска: `--exec-ffmpeg-log-level="error"`
    * с помощью переменной окружения: `#!shell export YUCCA_EXECUTOR_EXEC_FFMPEG_LOG_LEVEL="error"`
    * с помощью конфигурационного файла:

    ```toml
    # /opt/yucca/yucca.toml
    [executor.exec]
    ffmpeg_log_level = "error"
    ```

#### ffmpeg_log_max_file

Максимальное количество файлов журнала, которые могут присутствовать.

!!! note "По умолчанию `5`"
    Значение можно определить одним из способов:

    * с помощью аргумента запуска: `--exec-ffmpeg-log-max-file=5`
    * с помощью переменной окружения: `#!shell export YUCCA_EXECUTOR_EXEC_FFMPEG_LOG_MAX_FILE=5`
    * с помощью конфигурационного файла:

    ```toml
    # /opt/yucca/yucca.toml
    [executor.exec]
    ffmpeg_log_max_file = 5
    ```

#### ffmpeg_log_max_size

Максимальное размер файла журнала перед его ротацией и сжатием.

!!! note "По умолчанию `"20mb"`"
    Значение можно определить одним из способов:

    * с помощью аргумента запуска: `--exec-ffmpeg-log-max-size"20mb"`
    * с помощью переменной окружения: `#!shell export YUCCA_EXECUTOR_EXEC_FFMPEG_LOG_MAX_SIZE"20mb"`
    * с помощью конфигурационного файла:

    ```toml
    # /opt/yucca/yucca.toml
    [executor.exec]
    ffmpeg_log_max_size ="20mb"
    ```

#### hls_list_size

Длина m3u8 HLS плейлиста.

!!! note "По умолчанию `5`"
    Значение можно определить одним из способов:

    * с помощью аргумента запуска: `--exec-hls-list-size=5`
    * с помощью переменной окружения: `#!shell export YUCCA_EXECUTOR_EXEC_HLS_LIST_SIZE=5`
    * с помощью конфигурационного файла:

    ```toml
    # /opt/yucca/yucca.toml
    [executor.exec]
    hls_list_size = 5
    ```

#### hls_time

Желаемая длина сегмента видео HLS сегметов в секундах.

!!! note "По умолчанию `2`"
    Значение можно определить одним из способов:

    * с помощью аргумента запуска: `--exec-hls-time=2`
    * с помощью переменной окружения: `#!shell export YUCCA_EXECUTOR_EXEC_HLS_TIME=5`
    * с помощью конфигурационного файла:

    ```toml
    # /opt/yucca/yucca.toml
    [executor.exec]
    hls_time = 5
    ```

#### preview

Включение создания превью изображений в фоне.

!!! note "По умолчанию `true`"
    Значение можно определить одним из способов:

    * с помощью аргумента запуска: `--exec-preview=true`
    * с помощью переменной окружения: `#!shell export YUCCA_EXECUTOR_EXEC_PREVIEW=false`
    * с помощью конфигурационного файла:

    ```toml
    # /opt/yucca/yucca.toml
    [executor.exec]
    preview = true
    ```

#### preview_interval

Интервал создания превью изображений.

!!! note "По умолчанию `5s`"
    Значение можно определить одним из способов:

    * с помощью аргумента запуска: `--exec-preview-interval=5s`
    * с помощью переменной окружения: `#!shell export YUCCA_EXECUTOR_EXEC_PREVIEW_INTERVAL=5s`
    * с помощью конфигурационного файла:

    ```toml
    # /opt/yucca/yucca.toml
    [executor.exec]
    preview_interval = "5s"
    ```

#### probe_interval

Интервал с который поток начинает проверяться на доступность, интервал постепенно
возрастает(дабы избежать проблемы когда частые проверки приводят к зависанию IP камеры)
до значения, заданного в `probe_interval_maximum`, при этом количество попыток не должно
превышать значения `probe_retries`, иначе поток перейдет в статус *Failed*.

!!! note "По умолчанию `5s`"
    Значение можно определить одним из способов:

    * с помощью аргумента запуска: `--exec-probe-interval="5s"`
    * с помощью переменной окружения: `#!shell export YUCCA_EXECUTOR_EXEC_PROBE_INTERVAL="5s"`
    * с помощью конфигурационного файла:

    ```toml
    # /opt/yucca/yucca.toml
    [executor.exec]
    probe_interval = "5s"
    ```

#### probe_interval_maximum

Смотри описание `probe_interval`.

!!! note "По умолчанию `1m0s`"
    Значение можно определить одним из способов:

    * с помощью аргумента запуска: `--exec-probe-interval-maximum="1m0s"`
    * с помощью переменной окружения: `#!shell export YUCCA_EXECUTOR_EXEC_PROBE_INTERVAL_MAXIMUM="1m0s"`
    * с помощью конфигурационного файла:

    ```toml
    # /opt/yucca/yucca.toml
    [executor.exec]
    probe_interval_maximum = "1m0s"
    ```

#### probe_retries

Смотри описание `probe_interval`.

!!! note "По умолчанию `5`"
    Значение можно определить одним из способов:

    * с помощью аргумента запуска: `--exec-probe-retries=5`
    * с помощью переменной окружения: `#!shell export YUCCA_EXECUTOR_EXEC_PROBE_RETRIES=5`
    * с помощью конфигурационного файла:

    ```toml
    # /opt/yucca/yucca.toml
    [executor.exec]
    probe_retries = 5
    ```

#### probe_timeout

Таймаут получения информации по источнику.

!!! note "По умолчанию `5`"
    Значение можно определить одним из способов:

    * с помощью аргумента запуска: `--exec-probe-timeout="1m0s"`
    * с помощью переменной окружения: `#!shell export YUCCA_EXECUTOR_EXEC_PROBE_TIMEOUT="1m0s"`
    * с помощью конфигурационного файла:

    ```toml
    # /opt/yucca/yucca.toml
    [executor.exec]
    probe_timeout = "1m0s"
    ```

## cluster

Параметры относящиеся к настройке [кластера](/ru/usage/Cluster/) (распределённый режим).

### cluster_enabled

Включить режим кластера.

!!! note "По умолчанию `false`"
    Значение можно определить одним из способов:

    * с помощью аргумента запуска: `--cluster=true`
    * с помощью переменной окружения: `#!shell export YUCCA_CLUSTER_ENABLED=true`
    * с помощью конфигурационного файла:

    ```toml
    # /opt/yucca/yucca.toml
    [cluster]
    enabled = true
    ```

### cluster_advertise_address

Адрес для рекламы другим членам кластера.

!!! note "По умолчанию `127.0.0.1:9940`"
    Значение можно определить одним из способов:

    * с помощью аргумента запуска: `--cluster-advertise-address="127.0.0.1:9940"`
    * с помощью переменной окружения: `#!shell export YUCCA_CLUSTER_ADVERTISE_ADDRESS=24h`
    * с помощью конфигурационного файла:

    ```toml
    # /opt/yucca/yucca.toml
    [cluster]
    advertise_address = "127.0.0.1:9940"
    ```

### cluster_listen_address

Адрес для подключения членов кластера.

!!! note "По умолчанию `0.0.0.0:9940`"
    Значение можно определить одним из способов:

    * с помощью аргумента запуска: `--cluster-listen-address="0.0.0.0:9940"`
    * с помощью переменной окружения: `#!shell export YUCCA_CLUSTER_LISTEN_ADDRESS="0.0.0.0:9940"`
    * с помощью конфигурационного файла:

    ```toml
    # /opt/yucca/yucca.toml
    [cluster]
    listen_address = "0.0.0.0:9940"
    ```

## streams

Параметры относящиеся к поведению потоков.

### archive_download_max_duration

Максимальная допустимая длина запрашиваемого отрезка времени для скачивания.

!!! note "По умолчанию `24h`"
    Значение можно определить одним из способов:

    * с помощью аргумента запуска: `--streams-archive-download-max-duration=24h`
    * с помощью переменной окружения: `#!shell export YUCCA_STREAMS_ARCHIVE_DOWNLOAD_MAX_DURATION=24h`
    * с помощью конфигурационного файла:

    ```toml
    # /opt/yucca/yucca.toml
    [streams]
    archive_download_max_duration = "24h"
    ```

### archive_download_request_lifetime

Время жизни запрошенного для скачивания отрезка.  

При скачивании промежутков архива, формируется файл запрошенной длины и в течении времени указанного в этом параметре будет доступен для скачивания. Если вы загружаете длительные отрезки при медленной скорости скачивания, рекомендуется увеличить время в данном параметре.

!!! note "По умолчанию `1h0m0s`"
    Значение можно определить одним из способов:

    * с помощью аргумента запуска: `--streams-archive-download-request-lifetime=6h`
    * с помощью переменной окружения: `#!shell export YUCCA_STREAMS_ARCHIVE_DOWNLOAD_REQUEST_LIFETIME=1h0m0s`
    * с помощью конфигурационного файла:

    ```toml
    # /opt/yucca/yucca.toml
    [streams]
    archive_download_request_lifetime = "1h0m0s"
    ```

### archive_max_depth_hours

Максимальная допустимая глубина для хранения архива в часах.

!!! note "По умолчанию не ограничено `-1`"
    Значение можно определить одним из способов:

    * с помощью аргумента запуска: `--streams-archive-max-depth-hours=-1`
    * с помощью переменной окружения: `#!shell export YUCCA_STREAMS_ARCHIVE_MAX_DEPTH_HOURS=-1`
    * с помощью конфигурационного файла:

    ```toml
    # /opt/yucca/yucca.toml
    [streams]
    archive_max_depth_hours = -1
    ```

### live_on_demand_lease_duration

Время жизни аренды для живого потока по запросу (live on-demand).

Время в течении которого запрошенный live on-demand поток будет работать после последнего продления аренды, т.е. время жизни аренды после последнего её продления. Аренда продлевается при при запросе статуса о потоке на странице его просмотра. Чем меньше число тем быстрее поток перейдёт в состояние паузы после закрытия страницы просмотра.

!!! note "По умолчанию `1m0s`"
    Значение можно определить одним из способов:

    * с помощью аргумента запуска: `--streams-live-on-demand-lease-duration=1m0s`
    * с помощью переменной окружения: `#!shell export YUCCA_STREAMS_LIVE_ON_DEMAND_LEASE_DURATION=1m0s`
    * с помощью конфигурационного файла:

    ```toml
    # /opt/yucca/yucca.toml
    [streams]
    live_on_demand_lease_duration = 1m0s
    ```

### live_on_demand_lease_interval

Частота проверки активной аренды для живого потока по запросу (live on-demand).
Параметр должен быть меньше чем [live_on_demand_lease_duration](#live_on_demand_lease_duration).

!!! note "По умолчанию `30s`"
    Значение можно определить одним из способов:

    * с помощью аргумента запуска: `--streams-live-on-demand-lease-interval=30s`
    * с помощью переменной окружения: `#!shell export YUCCA_STREAMS_LIVE_ON_DEMAND_LEASE_INTERVAL=30s`
    * с помощью конфигурационного файла:

    ```toml
    # /opt/yucca/yucca.toml
    [streams]
    live_on_demand_lease_interval = 30s
    ```

### playlist_max_duration

Максимально допустимая длина запрашиваемого отрезка времени для M3U8-плейлиста.

!!! note "По умолчанию `3h`"
    Значение можно определить одним из способов:

    * с помощью аргумента запуска: `--streams-playlist-max-duration=3h`
    * с помощью переменной окружения: `#!shell export YUCCA_STREAMS_PLAYLIST_MAX_DURATION=3h`
    * с помощью конфигурационного файла:

    ```toml
    # /opt/yucca/yucca.toml
    [streams]
    playlist_max_duration = "3h"
    ```

### ranges_max_duration

Максимальная допустимая длина запрашиваемого отрезка времени для рейнджей (диапазон доступности архива).

!!! note "По умолчанию `168h`"
    Значение можно определить одним из способов:

    * с помощью аргумента запуска: `--streams-ranges-max-duration=168h`
    * с помощью переменной окружения: `#!shell export YUCCA_STREAMS_RANGES_MAX_DURATION=168h`
    * с помощью конфигурационного файла:

    ```toml
    # /opt/yucca/yucca.toml
    [streams]
    ranges_max_duration = "168h"
    ```

### uuid_slug

Использовать, в качестве публичного идентификатора потока, сгенерированный UUID.

!!! note "По умолчанию `false`"
    Значение можно определить одним из способов:

    * с помощью аргумента запуска: `--streams-uuid-slug=false`
    * с помощью переменной окружения: `#!shell export YUCCA_STREAMS_UUID_SLUG=false`
    * с помощью конфигурационного файла:

    ```toml
    # /opt/yucca/yucca.toml
    [streams]
    uuid_slug = false
    ```

## branding 💰

!!! info "💰 Доступно только в Enterprise"

Брендирование интерфейса.

### app_logo

Путь к файлу с логотипом. Можно указывать http(s) URL `http://` или путь на файловой системе `file://`.

!!! note "По умолчанию не определено `""`"
    Значение можно определить одним из способов:

    * с помощью аргумента запуска: `--branding-app-logo=https://example.com/logo.png`
    * с помощью переменной окружения: `#!shell export YUCCA_BRANDING_APP_LOGO=file:///opt/yucca/logo.png`
    * с помощью конфигурационного файла:

    ```toml
    # /opt/yucca/yucca.toml
    [branding]
    app_logo = "file:///opt/yucca/logo.png"
    ```

### app_title

Текст в поле Title.

!!! note "По умолчанию не определено `""`"
    Значение можно определить одним из способов:

    * с помощью аргумента запуска: `--branding-app-title=Yucca`
    * с помощью переменной окружения: `#!shell export YUCCA_BRANDING_APP_TITLE=Yucca`
    * с помощью конфигурационного файла:

    ```toml
    # /opt/yucca/yucca.toml
    [branding]
    app_title = "Yucca"
    ```

### site

Ссылка на ваш сайт.

!!! note "По умолчанию не определено `""`"
    Значение можно определить одним из способов:

    * с помощью аргумента запуска: `--branding-site=https://yucca.app`
    * с помощью переменной окружения: `#!shell export YUCCA_BRANDING_SITE=https://yucca.app`
    * с помощью конфигурационного файла:

    ```toml
    # /opt/yucca/yucca.toml
    [branding]
    site = "https://yucca.app"
    ```

### documentation

Ссылка на страницу с документацией.

!!! note "По умолчанию не определено `""`"
    Значение можно определить одним из способов:

    * с помощью аргумента запуска: `--branding-documentation=https://docs.yucca.app`
    * с помощью переменной окружения: `#!shell export YUCCA_BRANDING_DOCUMENTATION=https://docs.yucca.app`
    * с помощью конфигурационного файла:

    ```toml
    # /opt/yucca/yucca.toml
    [branding]
    documentation = "https://docs.yucca.app"
    ```

### fav_icon

Путь к файлу [Favicon](https://ru.wikipedia.org/wiki/Favicon). Можно указывать http(s) URL `http://` или путь на файловой системе `file://`.

!!! note "По умолчанию не определено `""`"
    Значение можно определить одним из способов:

    * с помощью аргумента запуска: `--branding-fav-icon=https://example.com/favicon.ico`
    * с помощью переменной окружения: `#!shell export YUCCA_BRANDING_FAV_ICON=https://example.com/favicon.ico`
    * с помощью конфигурационного файла:

    ```toml
    # /opt/yucca/yucca.toml
    [branding]
    fav_icon = "file:///opt/yucca/favicon.ico"
    ```

## quota 💰

!!! info "💰 Доступно только в Enterprise"

Глобальные квоты на ресурсы. Может ограничивать максимальное количество определённых ресурсов, которые можно создать/добавить в рамках инсталяции.

### global_stream

Квота на создание потоков.

!!! note "По умолчанию без ограничений `-1`"
    Значение можно определить одним из способов:

    * с помощью аргумента запуска: `--quota-global-stream=-1`
    * с помощью переменной окружения: `#!shell export YUCCA_QUOTA_GLOBAL_STREAM=-1`
    * с помощью конфигурационного файла:

    ```toml
    # /opt/yucca/yucca.toml
    [quota]
    global_stream = -1
    ```

### global_team

Квота на создание команд.

!!! note "По умолчанию без ограничений `-1`"
    Значение можно определить одним из способов:

    * с помощью аргумента запуска: `--quota-global-team=-1`
    * с помощью переменной окружения: `#!shell export YUCCA_QUOTA_GLOBAL_TEAM=-1`
    * с помощью конфигурационного файла:

    ```toml
    # /opt/yucca/yucca.toml
    [quota]
    global_team = -1
    ```

### global_user

Квота на создание пользователей.

!!! note "По умолчанию без ограничений `-1`"
    Значение можно определить одним из способов:

    * с помощью аргумента запуска: `--quota-global-user=-1`
    * с помощью переменной окружения: `#!shell export YUCCA_QUOTA_GLOBAL_USER=-1`
    * с помощью конфигурационного файла:

    ```toml
    # /opt/yucca/yucca.toml
    [quota]
    global_user = -1
    ```

### team_user

Квота на максимальное количество пользователей в команде.

!!! note "По умолчанию без ограничений `-1`"
    Значение можно определить одним из способов:

    * с помощью аргумента запуска: `--quota-team-user=-1`
    * с помощью переменной окружения: `#!shell export YUCCA_QUOTA_TEAM_USER=-1`
    * с помощью конфигурационного файла:

    ```toml
    # /opt/yucca/yucca.toml
    [quota]
    team_user = -1
    ```

## cookie

Параметры cookie.

### cookie_lifetime

Время жизни авторизационной сессии в cookie.

!!! note " По умолчанию `"720h0m0s"`"
    Значение можно определить одним из способов:

    * с помощью аргумента запуска: `--cookie-lifetime="720h0m0s"`
    * с помощью переменной окружения: `#!shell export YUCCA_COOKIE_LIFETIME="720h0m0s"`
    * с помощью конфигурационного файла:

    ```toml
    # /opt/yucca/yucca.toml
    [cookie]
    lifetime = "720h0m0s"
    ```

## security

Параметры безопасности.

### audit_logs

Включить аудит лог.

!!! note " По умолчанию `false`"
    Значение можно определить одним из способов:

    * с помощью аргумента запуска: `--security-audit-logs=false`
    * с помощью переменной окружения: `#!shell export YUCCA_SECURITY_AUDIT_LOGS=false`
    * с помощью конфигурационного файла:

    ```toml
    # /opt/yucca/yucca.toml
    [security]
    audit_logs = false
    ```

### brute_force_login_protection

Включить защиту от подбора пароля.

!!! note "По умолчанию `true`"
    Значение можно определить одним из способов:

    * с помощью аргумента запуска: `--security-brute-force-login-protection=true`
    * с помощью переменной окружения: `#!shell export YUCCA_SECURITY_BRUTE_FORCE_LOGIN_PROTECTION=true`
    * с помощью конфигурационного файла:

    ```toml
    # /opt/yucca/yucca.toml
    [security]
    brute_force_login_protection = true
    ```

### one_time_code_lifetime

Время действия одноразового пароля для 2FA и сброса пароля

!!! note "По умолчанию `"12h0m0s"`"
    Значение можно определить одним из способов:

    * с помощью аргумента запуска: `--security-one-time-code-lifetime="12h0m0s"`
    * с помощью переменной окружения: `#!shell export YUCCA_SECURITY_ONE_TIME_CODE_LIFETIME="12h0m0s"`
    * с помощью конфигурационного файла:

    ```toml
    # /opt/yucca/yucca.toml
    [security]
    one_time_code_lifetime = "12h0m0s"
    ```

### security_password_length

Минимальная допустимая длина пароля.

!!! note "По умолчанию `3`"
    Значение можно определить одним из способов:

    * с помощью аргумента запуска: `--security-password-length=3`
    * с помощью переменной окружения: `#!shell export YUCCA_SECURITY_PASSWORD_LENGTH=3`
    * с помощью конфигурационного файла:

    ```toml
    # /opt/yucca/yucca.toml
    [security]
    password_length = 3
    ```

### security_secret_key

Соль для подписи секретов и паролей.

!!! note "По умолчанию `"random string"`"
    Значение можно определить одним из способов:

    * с помощью аргумента запуска: `--security-secret-key="mWmPmpjFVesnXLxb6PBDcXPzyHva3c2p"`
    * с помощью переменной окружения: `#!shell export YUCCA_SECURITY_SECRET_KEY="mWmPmpjFVesnXLxb6PBDcXPzyHva3c2p"`
    * с помощью конфигурационного файла:

    ```toml
    # /opt/yucca/yucca.toml
    [security]
    secret_key = "mWmPmpjFVesnXLxb6PBDcXPzyHva3c2p"
    ```

## smtp_client

Настройки встроенного SMTP-клиента.
Отвечает за рассылку уведомлений пользователям по почте, например о том, что скоро заканчивается лицензия.

### enabled

Включить SMTP-клиент для отправки электронных писем.

!!! note "По умолчанию `false`"
    Значение можно определить одним из способов:

    * с помощью аргумента запуска: `--smtp-client=true`
    * с помощью переменной окружения: `#!shell export YUCCA_SMTP_CLIENT_ENABLED=true`
    * с помощью конфигурационного файла:

    ```toml
    # /opt/yucca/yucca.toml
    [smtp_client]
    enabled = true
    ```

### host

Адресс SMTP сервера.

!!! note "По умолчанию не определено `""`"
    Значение можно определить одним из способов:

    * с помощью аргумента запуска: `--smtp-client-host="smtp.gmail.com:465"`
    * с помощью переменной окружения: `#!shell export YUCCA_SMTP_CLIENT_HOST="smtp.yandex.ru:465"`
    * с помощью конфигурационного файла:

    ```toml
    # /opt/yucca/yucca.toml
    [smtp_client]
    host = ""
    ```

### user

Пользователь для авторизации на SMTP сервере.

!!! note "По умолчанию не определено `""`"
    Значение можно определить одним из способов:

    * с помощью аргумента запуска: `--smtp-client-user=""`
    * с помощью переменной окружения: `#!shell export YUCCA_SMTP_CLIENT_USER=""`
    * с помощью конфигурационного файла:

    ```toml
    # /opt/yucca/yucca.toml
    [smtp_client]
    user = ""
    ```

### password

Пароль для авторизации на SMTP сервере.

!!! note "По умолчанию не определено `""`"
    Значение можно определить одним из способов:

    * с помощью аргумента запуска: `--smtp-client-password=""`
    * с помощью переменной окружения: `#!shell export YUCCA_SMTP_CLIENT_PASSWORD=""`
    * с помощью конфигурационного файла:

    ```toml
    # /opt/yucca/yucca.toml
    [smtp_client]
    password = ""
    ```

### from_address

Адрес, используемый при отправке электронных писем.

!!! note "По умолчанию `"admin@yucca.localhost"`"
    Значение можно определить одним из способов:

    * с помощью аргумента запуска: `--smtp-client-from-address="admin@yucca.localhost"`
    * с помощью переменной окружения: `#!shell export YUCCA_SMTP_CLIENT_FROM_ADDRESS="admin@yucca.localhost"`
    * с помощью конфигурационного файла:

    ```toml
    # /opt/yucca/yucca.toml
    [smtp_client]
    from_address = "admin@yucca.localhost"
    ```

### from_name

Имя, используемое при отправке электронных писем.

!!! note "По умолчанию `"Yucca"`"
    Значение можно определить одним из способов:

    * с помощью аргумента запуска: `--smtp-client-from-name="Yucca"`
    * с помощью переменной окружения: `#!shell export YUCCA_SMTP_CLIENT_FROM_NAME="Yucca"`
    * с помощью конфигурационного файла:

    ```toml
    # /opt/yucca/yucca.toml
    [smtp_client]
    from_name = "Yucca"
    ```

### cert_path

Путь к файлу с сертификатом.

!!! note "По умолчанию не определено `""`"
    Значение можно определить одним из способов:

    * с помощью аргумента запуска: `--smtp-client-cert-path=""`
    * с помощью переменной окружения: `#!shell export YUCCA_SMTP_CLIENT_CERT_PATH=""`
    * с помощью конфигурационного файла:

    ```toml
    # /opt/yucca/yucca.toml
    [smtp_client]
    cert_path = ""
    ```

### key_path

Путь к файлу с ключём.

!!! note "По умолчанию не определено `""`"
    Значение можно определить одним из способов:

    * с помощью аргумента запуска: `--smtp-client-key-path=""`
    * с помощью переменной окружения: `#!shell export YUCCA_SMTP_CLIENT_KEY_PATH=""`
    * с помощью конфигурационного файла:

    ```toml
    # /opt/yucca/yucca.toml
    [smtp_client]
    key_path = ""
    ```

### skip_verify

Проверять SSL сертификат SMTP сервера.

!!! note "По умолчанию `false`"
    Значение можно определить одним из способов:

    * с помощью аргумента запуска: `--smtp-client-skip-verify=""`
    * с помощью переменной окружения: `#!shell export YUCCA_SMTP_CLIENT_SKIP_VERIFY=""`
    * с помощью конфигурационного файла:

    ```toml
    # /opt/yucca/yucca.toml
    [smtp_client]
    skip_verify = ""
    ```

## smtp_server

Настройки встроенного SMTP-сервера.

### enabled

Включить встроенный SMTP-сервер для получения события от камер.

!!! note "По умолчанию `true`"
    Значение можно определить одним из способов:

    * с помощью аргумента запуска: `--smtp-server=true`
    * с помощью переменной окружения: `#!shell export YUCCA_SMTP_SERVER_ENABLED=true`
    * с помощью конфигурационного файла:

    ```toml
    # /opt/yucca/yucca.toml
    [smtp_server]
    enabled = true
    ```

### listen_address

Адрес, на котором будет работать SMTP-сервер.

!!! note "По умолчанию `:1025`"
    Значение можно определить одним из способов:

    * с помощью аргумента запуска: `--smtp-server-listen-address=:1025`
    * с помощью переменной окружения: `#!shell export YUCCA_SMTP_SERVER_LISTEN_ADDRESS=:1025`
    * с помощью конфигурационного файла:

    ```toml
    # /opt/yucca/yucca.toml
    [smtp_server]
    listen_address = ":1025"
    ```

### password

Пароль для подключения к SMTP-серверу.

!!! note "По умолчанию не определено `""`"
    Значение можно определить одним из способов:

    * с помощью аргумента запуска: `--smtp-server-password=admin`
    * с помощью переменной окружения: `#!shell export YUCCA_SMTP_SERVER_PASSWORD=admin`
    * с помощью конфигурационного файла:

    ```toml
    # /opt/yucca/yucca.toml
    [smtp_server]
    password = "admin"
    ```

### username

Логин для подключения к SMTP-серверу.

!!! note "По умолчанию не определено `""`"
    Значение можно определить одним из способов:

    * с помощью аргумента запуска: `--smtp-server-username=admin`
    * с помощью переменной окружения: `#!shell export YUCCA_SMTP_SERVER_USERNAME=admin`
    * с помощью конфигурационного файла:

    ```toml
    # /opt/yucca/yucca.toml
    [smtp_server]
    username = "admin"
    ```

### read_timeout

Таймаут чтения входящего сообщения.

!!! note "По умолчанию `1s`"
    Значение можно определить одним из способов:

    * с помощью аргумента запуска: `--smtp-server-read-timeout=1s`
    * с помощью переменной окружения: `#!shell export YUCCA_SMTP_SERVER_READ_TIMEOUT=1s`
    * с помощью конфигурационного файла:

    ```toml
    # /opt/yucca/yucca.toml
    [smtp_server]
    read_timeout = "1s"
    ```

### event_window_duration

Длина нового события, детекции движения.

!!! note "По умолчанию `40s`"
    Значение можно определить одним из способов:

    * с помощью аргумента запуска: `--smtp-server-event-window-duration=40s`
    * с помощью переменной окружения: `#!shell export YUCCA_SMTP_SERVER_EVENT_WINDOW_DURATION=40s`
    * с помощью конфигурационного файла:

    ```toml
    # /opt/yucca/yucca.toml
    [smtp_server]
    event_window_duration = "40s"
    ```

## telemetry

Телеметрия.

### enabled

Включить предоставление метрик в формате Prometheus.

!!! note "По умолчанию `true`"
    Значение можно определить одним из способов:

    * с помощью аргумента запуска: `--telemetry=true`
    * с помощью переменной окружения: `#!shell export YUCCA_TELEMETRY_ENABLED=true`
    * с помощью конфигурационного файла:

    ```toml
    # /opt/yucca/yucca.toml
    [telemetry]
    enabled = true
    ```

### listen_address

IP адрес и порт на котором будет доступена телеметрия.

!!! note "По умолчанию `:9912`"
    Значение можно определить одним из способов:

    * с помощью аргумента запуска: `--telemetry-listen-address=:9912`
    * с помощью переменной окружения: `#!shell export YUCCA_TELEMETRY_LISTEN_ADDRESS=:9912`
    * с помощью конфигурационного файла:

    ```toml
    # /opt/yucca/yucca.toml
    [telemetry]
    listen_address = ":9912"
    ```

### path

URI для доступа к телеметрии.

!!! note "По умолчанию `/metrics`"
    Значение можно определить одним из способов:

    * с помощью аргумента запуска: `--telemetry-path=/metrics`
    * с помощью переменной окружения: `#!shell export YUCCA_TELEMETRY_PATH=/metrics`
    * с помощью конфигурационного файла:

    ```toml
    # /opt/yucca/yucca.toml
    [telemetry]
    path = "/metrics"
    ```

### basic_auth_username

Логин базовой авторизации для доступа к телеметрии.

!!! note "По умолчанию не определено `""`"
    Значение можно определить одним из способов:

    * с помощью аргумента запуска: `--telemetry-basic-auth-username=admin`
    * с помощью переменной окружения: `#!shell export YUCCA_TELEMETRY_BASIC_AUTH_USERNAME=admin`
    * с помощью конфигурационного файла:

    ```toml
    # /opt/yucca/yucca.toml
    [telemetry]
    basic_auth_username = "admin"
    ```

### basic_auth_password

Пароль базовой авторизации для доступа к телеметрии.

!!! note "По умолчанию не определено `""`"
    Значение можно определить одним из способов:

    * с помощью аргумента запуска: `--telemetry-basic-auth-password=admin`
    * с помощью переменной окружения: `#!shell export YUCCA_TELEMETRY_BASIC_AUTH_PASSWORD=admin`
    * с помощью конфигурационного файла:

    ```toml
    # /opt/yucca/yucca.toml
    [telemetry]
    basic_auth_password = "admin"
    ```

### environment_info

Лейблы для метрики `yucca_environment_info`.

!!! note "По умолчанию не определено `""`"
    Значение можно определить одним из способов:

    * с помощью аргумента запуска: `--telemetry-environment-info=env:test`
    * с помощью переменной окружения: `#!shell export YUCCA_TELEMETRY_ENVIRONMENT_INFO=env:test`
    * с помощью конфигурационного файла:

    ```toml
    # /opt/yucca/yucca.toml
    [telemetry.environment_info]
    env = "test"
    ```

## analytics

Сбор аналитики.

### yandex_metrika_counter_id

[Yandex.Metrika](https://metrika.yandex.ru/) Counter ID. Включено, если определено.

!!! note "По умолчанию не определено `""`"
    Значение можно определить одним из способов:

    * с помощью аргумента запуска: `--analytics-yandex-metrika-counter-id=12345678`
    * с помощью переменной окружения: `#!shell export YUCCA_ANALYTICS_YANDEX_METRIKA_COUNTER_ID=12345678`
    * с помощью конфигурационного файла:

    ```toml
    # /opt/yucca/yucca.toml
    [analytics]
    yandex_metrika_counter_id = 12345678
    ```

### yandex_metrika_webvisor

Включает вебвизор, подробные записи активности пользователей на сайте: движение мыши, прокрутка и клики.

!!! note "По умолчанию `false`"
    Значение можно определить одним из способов:

    * с помощью аргумента запуска: `--analytics-yandex-metrika-webvisor=true`
    * с помощью переменной окружения: `#!shell export YUCCA_ANALYTICS_YANDEX_METRIKA_WEBVISOR=true`
    * с помощью конфигурационного файла:

    ```toml
    # /opt/yucca/yucca.toml
    [analytics]
    yandex_metrika_webvisor = true
    ```
