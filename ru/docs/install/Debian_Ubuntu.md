---
title: Установка на Debian/Ubuntu
description: Видеонаблюдение для дома и бизнеса
---

# Установка на Debian/Ubuntu

!!! info "Помощь"
    Если вам нужна помощь специалиста для решения этой или любой другой задачи, изучите наши условия **[технической поддержки](/ru/support/)**.

!!! note "Используйте PostgreSQL, при большим количестве камер"
    При эксплуатации в производственных (production) средах с большим количеством камер (больше 10-ти)
    и включённой детекцией движения мы рекомендуем использовать в качестве базы данных PostgreSQL, как более производительную.
    Как установить и настроить использование, читайте [тут](/ru/usage/DB/postgres/)

??? question "Какую версию установить?"
    Существует 2 редакции Yucca  

    === "Free"
        Полностью бесплатная версия не требует лицензии, и не содержит расширенный функционал.

    === "Plus/Enterprise"
        Содержит расширенный функционал, требует покупки и использования лицензии.  
        Если у вас есть лицензия **Plus** или **Enterprise**, устанавливайте yucca с постфиксом `ent`.


Добавьте репозиторий и обновите список пакетов

```sh
(
sudo apt-get update
sudo apt-get install -y ca-certificates curl gnupg
curl -fsSL https://repo.yucca.app/apt/gpg.key | sudo gpg --dearmor -o /etc/apt/trusted.gpg.d/yucca.gpg
echo 'deb [signed-by=/etc/apt/trusted.gpg.d/yucca.gpg] https://repo.yucca.app/apt/ /' | sudo tee /etc/apt/sources.list.d/yucca.list
sudo apt update
)
```

Установите нужную версию Yucca

=== "Free"

    ```sh
    sudo apt install yucca
    ```

=== "Plus/Enterprise"

    ```sh
    sudo apt install yucca-ent
    ```

После запуска Web-интерфейс будет доступен по адресу <http://ip-вашего-сервера:9910>  

Полный список доступных версий, для установки вручную <a href="https://releases.yucca.app/" target="_blank">тут</a>.  

## Дополнительно

* [Системные требования](/ru/usage/SystemRequirements/)
* [Конфигурация](/ru/configuration/)
