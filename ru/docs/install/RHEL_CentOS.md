---
title: Установка на RHEL/CentOS
description: Видеонаблюдение для дома и бизнеса
---

# Установка на RHEL/CentOS

!!! info "Помощь"
    Если вам нужна помощь специалиста для решения этой или любой другой задачи, изучите наши условия **[технической поддержки](/ru/support/)**.

!!! note "Используйте PostgreSQL, при большим количестве камер"
    При эксплуатации в производственных (production) средах с большим количеством камер (больше 10-ти)
    и включённой детекцией движения мы рекомендуем использовать в качестве базы данных PostgreSQL, как более производительную.
    Как установить и настроить использование, читайте [тут](/ru/usage/DB/postgres/)

??? question "Какую версию установить?"
    Существует 2 редакции Yucca  

    === "Free"
        Полностью бесплатная версия не требует лицензии, и не содержит расширенный функционал.

    === "Plus/Enterprise"
        Содержит расширенный функционал, требует покупки и использования лицензии.  
        Если у вас есть лицензия **Plus** или **Enterprise**, устанавливайте yucca с постфиксом `ent`.


Добавьте репозиторий

```sh 
sudo tee /etc/yum.repos.d/yucca.repo <<EOF
[yucca]
name=Yucca
baseurl=https://repo.yucca.app/yum/
enabled=1
gpgcheck=0
EOF
```

Установите нужную версию Yucca

=== "Free"

    ```sh
    sudo yum install yucca
    ```

=== "Plus/Enterprise"

    ```sh
    sudo yum install yucca-ent
    ```

Откройте порт в firewalld:

```bash
sudo firewall-cmd --permanent --new-service=yucca
sudo firewall-cmd --permanent --service=yucca --add-port=9910/tcp
sudo firewall-cmd --permanent --zone=public --add-service=yucca
sudo firewall-cmd --reload
```

После запуска Web-интерфейс будет доступен по адресу <http://ip-вашего-сервера:9910>

Полный список доступных версий, для установки вручную <a href="https://releases.yucca.app/" target="_blank">тут</a>.  

## Дополнительно

* [Системные требования](/ru/usage/SystemRequirements/)
* [Конфигурация](/ru/configuration/)