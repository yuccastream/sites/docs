---
title: Установка
description: Видеонаблюдение для дома и бизнеса
---

# Установка

## Обзор

Ознакомьтесь со следующими документами по установке:

- [Docker](/ru/install/Docker/)
- [Debian/Ubuntu](/ru/install/Debian_Ubuntu/)
- [SUSE](/ru/install/SUSE/)
- [RHEL/CentOS](/ru/install/RHEL_CentOS/)
- [Other Linux](/ru/install/other_linux/)
- [macOS](/ru/install/macOS/)

## Дополнительно

- [Системные требования](/ru/usage/SystemRequirements/)
- [Конфигурация](/ru/configuration/)
