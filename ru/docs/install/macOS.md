---
title: Установка на macOS
description: Видеонаблюдение для дома и бизнеса
---

# Установка на macOS

!!! info "Помощь"
    Если вам нужна помощь специалиста для решения этой или любой другой задачи, изучите наши условия **[технической поддержки](/ru/support/)**.

!!! notice "Работа Yucca на новых ARM-процессорах Apple M1"
    10 ноября 2020 года компания Apple [анонсировала](https://www.apple.com/mac/m1/) выход устройств на базе нового процессора, а для обеспечения обратной совместимости был представлен эмулятор [Rosetta 2](https://developer.apple.com/documentation/apple_silicon/about_the_rosetta_translation_environment).

    Мы не проводили тестирование Yucca на новых процессорах и не гарантируем корректную работу через эмулятор.

!!! note "Зависимости"
    1. Установите [Homebrew](https://brew.sh/index_ru)
    2. Установите `wget`

    ```shell
    brew install wget
    ```

??? question "Какую версию установить?"
    Существует 2 редакции Yucca  

    === "Free"
        Полностью бесплатная версия не требует лицензии, и не содержит расширенный функционал.

    === "Plus/Enterprise"
        Содержит расширенный функционал, требует покупки и использования лицензии.  
        Если у вас есть лицензия **Plus** или **Enterprise**, устанавливайте yucca с постфиксом `ent`.

## Установка с Homebrew

Откройте терминал, выполните команды:

```shell
(
brew tap yuccastream/tap
brew install yucca
)
```

## Установка без Homebrew

Откройте терминал, скачайте нужные файлы и запустите сервер:

=== "Free"

    ```shell
    (
    mkdir -p ./yucca
    cd ./yucca
    wget https://releases.yucca.app/latest/yucca_darwin_amd64.tar.gz
    tar -xzvf yucca_darwin_amd64.tar.gz
    rm -f yucca_darwin_amd64.tar.gz
    chmod +x ./yucca
    ./yucca server
    )
    ```

=== "Plus/Enterprise"

    ```shell
    (
    mkdir -p ./yucca
    cd ./yucca
    wget https://releases.yucca.app/latest/yucca-ent_darwin_amd64.tar.gz
    tar -xzvf yucca-ent_darwin_amd64.tar.gz
    rm -f yucca-ent_darwin_amd64.tar.gz
    chmod +x ./yucca
    ./yucca server
    )
    ```

После запуска Web-интерфейс будет доступен по адресу http://ip-вашего-сервера:9910

## Дополнительно

* [Системные требования](/ru/usage/SystemRequirements/)
* [Конфигурация](/ru/configuration/)
