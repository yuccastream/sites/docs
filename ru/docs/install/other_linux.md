---
title: Установка вручную на любые Linux
description: Видеонаблюдение для дома и бизнеса
---

# Установка вручную на любые Linux

!!! info "Помощь"
    Если вам нужна помощь специалиста для решения этой или любой другой задачи, изучите наши условия **[технической поддержки](/ru/support/)**.

!!! note "Используйте PostgreSQL, при большим количестве камер"
    При эксплуатации в производственных (production) средах с большим количеством камер (больше 10-ти)
    и включённой детекцией движения мы рекомендуем использовать в качестве базы данных PostgreSQL, как более производительную.
    Как установить и настроить использование, читайте [тут](/ru/usage/DB/postgres/)

??? question "Какую версию установить?"
    Существует 2 редакции Yucca  

    === "Free"
        Полностью бесплатная версия не требует лицензии, и не содержит расширенный функционал.

    === "Plus/Enterprise"
        Содержит расширенный функционал, требует покупки и использования лицензии.  
        Если у вас есть лицензия **Plus** или **Enterprise**, устанавливайте yucca с постфиксом `ent`.

## Установка вручную

Выполните `arch`, чтобы узнать свою архитектуру

| Вывод `arch` | Обозначение в документации|
|:---:|:---:|
| x86_64 | amd64 |
| armv7l | arm |
| aarch64 | arm64 |

Создаём каталог для установки и переходим в него, скачиваем последнюю версию Yucca:

=== "Free"
    === "amd64"

        ```bash
        (
        sudo mkdir -p /opt/yucca/ffmpeg
        cd /opt/yucca
        sudo wget https://releases.yucca.app/latest/yucca_linux_amd64.tar.gz
        sudo tar -xzvf yucca_linux_amd64.tar.gz
        sudo rm -f yucca_linux_amd64.tar.gz
        )
        ```

    === "arm"

        ```bash
        (
        sudo mkdir -p /opt/yucca/ffmpeg
        cd /opt/yucca
        sudo wget https://releases.yucca.app/latest/yucca_linux_arm.tar.gz
        sudo tar -xzvf yucca_linux_arm.tar.gz
        sudo rm -f yucca_linux_arm.tar.gz
        )
        ```

    === "arm64"

        ```bash
        (
        sudo mkdir -p /opt/yucca/ffmpeg
        cd /opt/yucca
        sudo wget https://releases.yucca.app/latest/yucca_linux_arm64.tar.gz
        sudo tar -xzvf yucca_linux_arm64.tar.gz
        sudo rm -f yucca_linux_arm64.tar.gz
        )
        ```

=== "Plus/Enterprise"
    === "amd64"

        ```bash
        (
        sudo mkdir -p /opt/yucca/ffmpeg
        cd /opt/yucca
        sudo wget https://releases.yucca.app/latest/yucca-ent_linux_amd64.tar.gz
        sudo tar -xzvf yucca-ent_linux_amd64.tar.gz
        sudo rm -f yucca-ent_linux_amd64.tar.gz
        )
        ```

    === "arm"

        ```bash
        (
        sudo mkdir -p /opt/yucca/ffmpeg
        cd /opt/yucca
        sudo wget https://releases.yucca.app/latest/yucca-ent_linux_arm.tar.gz
        sudo tar -xzvf yucca-ent_linux_arm.tar.gz
        sudo rm -f yucca-ent_linux_arm.tar.gz
        )
        ```

    === "arm64"

        ```bash
        (
        sudo mkdir -p /opt/yucca/ffmpeg
        cd /opt/yucca
        sudo wget https://releases.yucca.app/latest/yucca-ent_linux_arm64.tar.gz
        sudo tar -xzvf yucca-ent_linux_arm64.tar.gz
        sudo rm -f yucca-ent_linux_arm64.tar.gz
        )
        ```

<a href="https://docs.yucca.app/releases" target="_blank">Полный список доступных версий</a>

Генерируем файл конфигурации со значениями по умолчанию:

```sh
sudo /opt/yucca/yucca server --config emtpy --show-config | sed 's|data_dir = ""|data_dir = "/opt/yucca/data"|' > /opt/yucca/yucca.toml
```

Создаём пользователя и задаем владельца и права доступа:

```sh
(
sudo groupadd --gid 642 --force yucca
sudo useradd --system -u 642 --no-create-home --shell /bin/false --home-dir /opt/yucca --gid yucca yucca
sudo chown -R yucca:yucca /opt/yucca
sudo chmod -R 2775 /opt/yucca
)
```

Дополнительно можно указать свой путь для сохранения архива определив параметр [alloc_dir](/ru/configuration/#alloc_dir), предварительно создайте нужный каталог, пример:

!!! info "В вашем случае путь может быть иной!"

    ```sh
    (
    sudo mkdir -p /volume1/yucca/alloc_dir
    sudo chown -R yucca:yucca /volume1/yucca/alloc_dir
    sudo chmod -R 2775 /volume1/yucca/alloc_dir
    sed -i 's|alloc_dir = ""|alloc_dir = "/volume1/yucca/alloc_dir"|' /opt/yucca/yucca.toml
    )
    ```

    Убедитесь, что всё корректно:

    ```sh
    cat /opt/yucca/yucca.toml
    ```

## Systemd автозапуск

Создаём systemd unit файл:

```bash
sudo tee /lib/systemd/system/yucca.service <<EOF
[Unit]
Description=Yucca https://yucca.app
Documentation=https://docs.yucca.app
After=syslog.target network.target remote-fs.target nss-lookup.target

[Service]
Type=simple
User=yucca
Group=yucca
SyslogIdentifier=yucca
PIDFile=/run/yucca.pid
LimitNOFILE=1024
WorkingDirectory=/opt/yucca
ExecStart=/opt/yucca/yucca server --config /opt/yucca/yucca.toml
ExecStop=/bin/kill -s SIGTERM $MAINPID
Restart=on-failure
RestartSec=10s

[Install]
WantedBy=multi-user.target
EOF

```

Теперь можно запустить сервер и проверить работу:

```bash
sudo systemctl daemon-reload
sudo systemctl enable yucca
sudo systemctl start yucca
```

## Upstart автозапуск

!!! example ""
    К примеру используется в Synology NAS

Создаём скрипт автозапуска Upstart init:

```sh
sudo tee /etc/init/yucca.conf <<EOF
description "Yucca https://yucca.app"
author "Yucca"

start on filesystem or runlevel [2345]
stop on runlevel [!2345]

respawn
respawn limit 10 10
setuid yucca
setgid yucca
chdir /opt/yucca
limit nofile 65535 65535
script
    exec /opt/yucca/yucca server --config /opt/yucca/yucca.toml
end script

EOF
```

Обновляем конфигурацию Upstart, проверяем, запускаем:

```sh
sudo initctl reload-configuration
sudo initctl list | grep yucca
sudo initctl start yucca
```

## OpenRC/sysvinit автозапуск

Инструкции нет, вы можете её предложить.

## Проверка работы

После запуска Web-интерфейс будет доступен по адресу <http://ip-вашего-сервера:9910>

## Дополнительно


* [Системные требования](/ru/usage/SystemRequirements/)
* [Конфигурация](/ru/configuration/)
