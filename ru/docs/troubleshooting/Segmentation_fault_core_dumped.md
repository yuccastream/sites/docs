---
title: Segmentation fault (core dumped)
description: Видеонаблюдение для дома и бизнеса
---

# Segmentation fault (core dumped)

!!! info ""
    Если вам нужна помощь специалиста для решения этой или любой другой проблемы, изучите наши условия **[технической поддержки](/ru/support/)**

## Описание

Данная ошибка может возникать при попытке добавить поток

[![segmentation-fault-core-dumped](/ru/media/troubleshooting/Segmentation fault (core dumped).png)](/ru/media/troubleshooting/Segmentation fault (core dumped).png)

также можно увидеть в логе сервера

```sh
WARN    latest/runner.go:308    Failed to create preview: signal: segmentation fault (core dumped)
```

## Решение

Для устранения проблемы, нужно установить пакет **nscd**

в ubuntu/debian:

```sh
sudo apt install nscd
```

в fedora/centos:

```sh
dnf установить nscd
```

## Причина

Для Linux мы используем статическую сборку ffmpeg от <https://www.johnvansickle.com/ffmpeg/>.
В readme.txt к сборке есть заметка:

```txt
Notes:  A limitation of statically linking glibc is the loss of DNS resolution. Installing
        nscd through your package manager will fix this.
```
