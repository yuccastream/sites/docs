---
title: Too many open files
description: Видеонаблюдение для дома и бизнеса
---

# Too many open files

!!! info ""
    Если вам нужна помощь специалиста для решения этой или любой другой проблемы, изучите наши условия **[технической поддержки](/ru/support/)**.

## Описание

При большом количестве камер, в районе 128, вы можете столкнуться с ограничениями [Inotify](https://ru.wikipedia.org/wiki/Inotify), тогда при добавлении камеры в логе сервера можно увидеть сообщение:

```sh
panic: runtime error: invalid memory address or nil pointer dereference
[signal SIGSEGV: segmentation violation code=0x1 addr=0xe8 pc=0xdda663]

goroutine 1258 [running]:
gitlab.com/yuccastream/yucca/pkg/runner/latest.(*Runner).GetStats(...)
        /builds/yuccastream/yucca/pkg/runner/latest/runner.go:327
gitlab.com/yuccastream/yucca/pkg/server/executor/exec.(*Worker).GetStream(0xc002b74500)
        /builds/yuccastream/yucca/pkg/server/executor/exec/worker.go:188 +0xa3
gitlab.com/yuccastream/yucca/pkg/server/executor/exec.(*Executor).StatusStream(0xc0003f01c0, 0xc00074b6c0)
        /builds/yuccastream/yucca/pkg/server/executor/exec/executor.go:126 +0xb8
gitlab.com/yuccastream/yucca/pkg/server.(*Server).stateWatcher(0xc000c1e000)
        /builds/yuccastream/yucca/pkg/server/server.go:914 +0x233
created by gitlab.com/yuccastream/yucca/pkg/server.(*Server).Start in goroutine 1
        /builds/yuccastream/yucca/pkg/server/server.go:695 +0x4db
```

А логе одной из камер:

```sh
Error: too many open files
```

По умолчанию во многих Linux дистрибутивах выставлены следующие параметры:

```bash
sysctl fs.inotify

fs.inotify.max_queued_events = 16384
fs.inotify.max_user_instances = 128
fs.inotify.max_user_watches = 524288

ulimit -n
10240

grep NOFILE /etc/systemd/system.conf
#DefaultLimitNOFILE=1024:524288
```

## Решение

Чтобы увеличить лимиты, добавьте в файл `/etc/sysctl.conf` следующие параметры:

```bash
sudo su
cat << EOF >> /etc/sysctl.conf
fs.inotify.max_queued_events = 16384
fs.inotify.max_user_instances = 10240
fs.inotify.max_user_watches = 524288
EOF
```

в файле `/etc/systemd/system.conf`:

```bash
DefaultLimitNOFILE=10240:524288
```

и перезагрузите систему.

Убедитесь, что настройки применены:

```bash
ulimit -n
10240

sysctl fs.inotify
fs.inotify.max_queued_events = 16384
fs.inotify.max_user_instances = 10240
fs.inotify.max_user_watches = 524288
```
