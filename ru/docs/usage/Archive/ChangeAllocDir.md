---
title: Изменение пути для хранения архива
description: Видеонаблюдение для дома и бизнеса
---

# Изменение пути для хранения архива

По умолчанию архив с камер сохраняется в каталоге `/opt/yucca/data/alloc/`, путь определяется параметром [alloc_dir](/ru/configuration/#alloc_dir). Если вы хотите изменить это поведение, создайте каталог по новому пути и примените к нему права пользователя от имени которого запускается Yucca, по умолчанию это пользователь `yucca`, например:

```sh
sudo mkdir -p /mnt/external_drive_8TB/yucca_alloc
sudo chown yucca:yucca /mnt/external_drive_8TB/yucca_alloc
```

!!! info "Монтирование диска"
    Если вы смонтировали отдельный диск, убедитесь, что он отображается в списке – `df -h`, так же не забудьте добавить запись о монтировании в `/etc/fstab`, чтобы сервер автоматически монтировал диск при перезагрузке.

Остановите Yucca сервер:

```sh
sudo systemctl stop yucca
```

Замените параметр в [alloc_dir](/ru/configuration/#alloc_dir) в конфигурации:

=== "Файл конфигурации"

    ```sh
    alloc_dir = "/mnt/external_drive_8TB/yucca_alloc"
    ```

=== "Переменная окружения"

    ```sh
    YUCCA_SERVER_ALLOC_DIR=/mnt/external_drive_8TB/yucca_alloc
    ```

=== "Аргумент командной строки"

    ```sh
    --alloc-dir=/mnt/external_drive_8TB/yucca_alloc
    ```

Запустите Yucca сервер:

```sh
sudo systemctl start yucca
```

Убедитесь, что в новом каталоге создаются файлы, а в веб интерфейсе на вкладке с конфигурацией отображается новый путь.
