---
title: Расширение каталога с архивом на несколько дисков с помощью MergerFS
description: Видеонаблюдение для дома и бизнеса
---

!!! info "Используйте MergerFS только если не можете заранее спланировать дисковый массив"
    Мы рекомендуем заранее планировать пространство под архив и для больших объёмов данных использовать специальные технологии,
    такие как [ZFS](https://openzfs.github.io/openzfs-docs/index.html), которые позволяют равномерно балансировать данные по дискам, имеют систему отказоустойчивости и могут без проблем расширять массивы данных на десятки дисков и сотни терабайт данных.

## Проблема

Вы запустили Yucca в базовой конфигурации с архивом на диске с системой, со временем количество камер и архива увеличилось и текущего диска уже не хватает. Можно добавить в систему ещё один или несколько дисков и равномерно распределить хранение архива по ним – это потребует остановки Yucca, создания дискового массива, переноса всех данных. В этом случае сервис может быть недоступен несколько часов, что недопустимо.

## Решение

В случае если вы хотите постепенно расширять каталог для хранения архива добавляя дополнительные диски, но без использования RAID технологий, вроде [ZFS](https://openzfs.github.io/openzfs-docs/index.html) или [mdadm](https://ru.wikipedia.org/wiki/Mdadm), и не перестраивать всю инфраструктуру, можно использовать объединённые файловые системы – Union filesystem.

Есть несколько решений в данном сегменте, но мы остановимся на [mergerfs](https://github.com/trapexit/mergerfs) и далее всё будет именно в её контексте. Сравнение mergerfs с другими решениями доступно [здесь](https://github.com/trapexit/mergerfs?tab=readme-ov-file#mergerfs-versus-x).

mergerfs - это объединенная файловая система, предназначенная для упрощения хранения и управления файлами на многочисленных обычных устройствах хранения, он похож на mhddfs, UnionFS и Aufs. Больше подробностей вы можете найти в официальном репозитории проекта - [https://github.com/trapexit/mergerfs](https://github.com/trapexit/mergerfs).

mergerfs, грубо говоря, создаёт виртуальный каталог, под которым может находиться несколько реальных каталогов на разных дисках с разными файловыми системами, вроде ext4 или XFS. mergerfs не разбивает файлы на части, они полностью со всеми метаданными хранятся в одном из подкаталогов:

```sh
A         +      B        =       C
/disk1           /disk2           /merged
|                |                |
+-- /dir1        +-- /dir1        +-- /dir1
|   |            |   |            |   |
|   +-- file1    |   +-- file2    |   +-- file1
|                |   +-- file3    |   +-- file2
+-- /dir2        |                |   +-- file3
|   |            +-- /dir3        |
|   +-- file4        |            +-- /dir2
|                     +-- file5   |   |
+-- file6                         |   +-- file4
                                  |
                                  +-- /dir3
                                  |   |
                                  |   +-- file5
                                  |
                                  +-- file6
```

### Плюсы

- Легко расширять пространство добавляя новые диски;
- Легко настроить, mergerfs - файловая система пространства пользователя, поэтому вы можете установить её как программу на любой Linux;
- Нет специальных требований к объединяемым каталогам, это могут быть диски с разным объёмом, файловыми системами или вообще сетевые папки;
- Не требует перестройки всей инфраструктуры.

### Минусы

- Нет балансировки нагрузки – нагрузка на одни диски может быть выше чем на другие, как итог архив с одних камер может отдаваться медленнее чем с других;
- Нет избыточности, отказ диска повлечёт полную потерю данных.

## Установка

Инструкция по установке есть в [репозитории](https://github.com/trapexit/mergerfs?tab=readme-ov-file#install).

!!! node "Заметка"
    Если менеджер пакетов вашего дистрибутива включает mergerfs, проверьте, актуальна ли его версия. Если версия устарела, рекомендуется использовать последний выпуск, найденный на [странице](https://github.com/trapexit/mergerfs/releases). Подробности для распространенных дистрибутивов приведены ниже.

Описанные ниже действия производились на Ubuntu 22.04, mergerfs был установлен через deb-пакет из [репозитория](https://github.com/trapexit/mergerfs/releases).

Для openSUSE есть [репозиторий в OBS](https://software.opensuse.org//download.html?project=filesystems&package=mergerfs) с последними версиями.

## Настройка

В примере виртуальная машина с диском на 20 гигабайт и двумя дополнительных диска на 10 гигабайт:

```sh
# df -h | grep -v -E "(tmpfs|overlay)"
Filesystem      Size  Used Avail Use% Mounted on
/dev/sda1        19G  4.5G   14G  26% /
/dev/sda15      253M  142K  252M   1% /boot/efi
/dev/sdb        9.8G   28K  9.3G   1% /mnt/HC_Volume_100511911
/dev/sdc        9.8G   28K  9.3G   1% /mnt/HC_Volume_100511912

# lsblk 
NAME    MAJ:MIN RM  SIZE RO TYPE MOUNTPOINTS
sda       8:0    0 19.1G  0 disk 
├─sda1    8:1    0 18.8G  0 part /
├─sda14   8:14   0    1M  0 part 
└─sda15   8:15   0  256M  0 part /boot/efi
sdb       8:16   0   10G  0 disk /mnt/HC_Volume_100511911
sdc       8:32   0   10G  0 disk /mnt/HC_Volume_100511912
```

На сервере работает Yucca, добавлены 6 камер с архивом:

```sh
cat /opt/yucca/yucca.toml | grep "alloc_dir ="
  alloc_dir = "/opt/yucca/data/alloc"
```

Данные в архив успешно записываются:

```sh
# tree /opt/yucca/data/alloc/ | head
/opt/yucca/data/alloc/
├── 2
│   ├── archive
│   │   └── 2024
│   │       └── 03
│   │           └── 27
│   │               ├── 15.dvr
│   │               └── 15.ranges
│   ├── logs
│   │   └── ffmpeg.log
│   └── segment
...
```

Теперь внутри точек монтирования с нашими дополнительными дискам создадим каталог который мы примонтируем и назначим права, чтобы Yucca могла писать туда:

```sh
mkdir -p /mnt/HC_Volume_100511911/yucca
mkdir -p /mnt/HC_Volume_100511912/yucca
chown yucca:yucca /mnt/HC_Volume_100511911/yucca
chown yucca:yucca /mnt/HC_Volume_100511912/yucca
```

Создаём каталог, в который мы будем производить монтирование:

```sh
mkdir -p /mnt/yucca_mergerfs
```

Монтируем объединённый том:

```sh
mergerfs -o fsname=mergerfs,cache.files=partial,dropcacheonclose=true,category.create=mfs,moveonenospc=true,minfreespace=20G /mnt/HC_Volume_100511911/yucca:/mnt/HC_Volume_100511912/yucca:/opt/yucca/data/alloc /mnt/yucca_mergerfs
```

О значение всех параметров можно прочитать почитать в [официальной документации](https://github.com/trapexit/mergerfs). Проверяем, что том смонтирован:

!!! info "Заметьте"
    Итоговый том имеет размер равный сумме всех объединённых томов.

```sh
# df -h | grep -v -E "(tmpfs|overlay)"
Filesystem                                                                        Size  Used Avail Use% Mounted on
/dev/sda1                                                                          19G  5.2G   13G  30% /
/dev/sda15                                                                        253M  142K  252M   1% /boot/efi
/dev/sdb                                                                          9.8G   28K  9.3G   1% /mnt/HC_Volume_100511911
/dev/sdc                                                                          9.8G   28K  9.3G   1% /mnt/HC_Volume_100511912
mnt/HC_Volume_100511911/yucca:mnt/HC_Volume_100511912/yucca:opt/yucca/data/alloc   38G  5.2G   31G  15% /mnt/yucca_mergerfs
```

Проверяем, что в каталогах `/mnt/yucca_mergerfs` и `/opt/yucca/data/alloc` одинаковый набор файлов:

```sh
ls /mnt/yucca_mergerfs/
2  3  4  5  6  8

ls /opt/yucca/data/alloc/
2  3  4  5  6  8
```

Теперь, с помощью любого текстового редактора, изменяем параметр [alloc_dir](/ru/configuration/#alloc_dir):

```sh
# sed -i 's|/opt/yucca/data/alloc|/mnt/yucca_mergerfs|' /opt/yucca/yucca.toml
# cat /opt/yucca/yucca.toml | grep "alloc_dir ="
  alloc_dir = "/mnt/yucca_mergerfs"
```

Перезапускаем Yucca сервер и проверяем, что всё в порядке:

```sh
systemctl restart yucca
systemctl status yucca
```

Yucca пишет сегменты часовыми отрезками, значит на новых дисках `/mnt/HC_Volume_100511911/yucca` и `/mnt/HC_Volume_100511912/yucca` данные должны появиться, только с наступлением нового часа.

```sh
root@ubuntu-2gb-fsn1-1:~# tree /mnt/HC_Volume_100511911/yucca/ | head
/mnt/HC_Volume_100511911/yucca/
├── 2
│   ├── archive
│   └── logs
│       └── ffmpeg.log
├── 3
│   ├── archive
│   │   └── 2024
│   │       └── 03
│   │           └── 27
root@ubuntu-2gb-fsn1-1:~# tree /mnt/HC_Volume_100511912/yucca/ | head
/mnt/HC_Volume_100511912/yucca/
├── 2
│   ├── archive
│   │   └── 2024
│   │       └── 03
│   │           └── 27
│   │               ├── 16.dvr
│   │               └── 16.ranges
│   ├── logs
│   └── segment

```

Чтобы объединённый каталог был смонтирован при перезагрузке системы, добавляем запись в `/etc/fstab`:

```sh
# yucca mergerfs
/mnt/HC_Volume_100511911/yucca:/mnt/HC_Volume_100511912/yucca:/opt/yucca/data/alloc /mnt/yucca_mergerfs mergerfs cache.files=partial,dropcacheonclose=true,category.create=mfs,moveonenospc=true,minfreespace=20G 0 0
```

Проверяем, что нет ошибок:

```sh
mount -a
```

Перезагружаем систему для проверки.

## Заключение

Мы не тестировали mergerfs под большими нагрузками или в других исключительных ситуациях, но можем сказать, что, в общем случае, всё работает стабильно и проблем не наблюдается. Если проблемы все же возникли, обратитесь к [трекеру](https://github.com/trapexit/mergerfs/issues) проекта, там же вы может финансово [поддержать](https://github.com/trapexit/mergerfs?tab=readme-ov-file#donations) разработчиков.
