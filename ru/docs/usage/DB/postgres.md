---
title: Установка Postgres
description: Видеонаблюдение для дома и бизнеса
---

# Установка Postgres

!!! info ""
    Если вам нужна помощь специалиста для решения этой или любой другой проблемы, изучите наши условия **[технической поддержки](/ru/support/)**

Есть множество способов как установить СУБД PostgreSQL, мы опишем 2 по нашему мнению самых простых и быстрых.
**Вам нужно выбрать наиболее подходящий для вас.**

!!! warning "В примерах использован простой пароль, его желательно изменить!"

## Установка PostgreSQL из пакетов на хост

Для установки используйте официальную инструкцию с сайта разработчика <https://www.postgresql.org/download/>

!!! info "Быстрая установка в Debian или Ubuntu:"

    ```bash
    sudo apt install postgresql-common
    sudo sh /usr/share/postgresql-common/pgdg/apt.postgresql.org.sh
    sudo apt install postgresql-14 -y
    ```

После установки и запуска, нужно создать базу данных и роль для подключения.

```bash
sudo -u postgres psql -c "CREATE DATABASE yucca;"
sudo -u postgres psql -c "CREATE USER postgres WITH PASSWORD 'postgres';"
sudo -u postgres psql -c 'GRANT ALL PRIVILEGES ON DATABASE yucca TO postgres;'
```

## Установка PostgreSQL в docker

Запустите официальный контейнер с нужными параметрами:

```bash
docker run -d \
    --name yucca-postgres \
    --net host \
    --restart always \
    -e POSTGRES_USER=postgres \
    -e POSTGRES_PASSWORD=postgres \
    -e POSTGRES_DB=yucca \
    postgres:14.4-alpine
```

## Настройка подключения Yucca к базе Postgres

Укажите тип `postgres` в параметре [database.type](/ru/configuration/#type), а так же остальные реквизиты для подключения:

- [database.host](/ru/configuration/#host)
- [database.name](/ru/configuration/#name)
- [database.user](/ru/configuration/#user)
- [database.password](/ru/configuration/#password)

пример конечной конфигурации через файл:

```toml
[database]
  host = "127.0.0.1:5432"
  name = "yucca"
  password = "postgres"
  type = "postgres"
  user = "postgres"
```

Перезапустите сервер:

```bash
sudo systemctl restart yucca
```

При успешном подключении, в логе (вкладка debug) можно увидеть примерно следующие сообщения:

```bash
sudo journalctl -o short --no-pager -n 100 -f -u yucca | grep "ORM"
...
2022-08-12T12:10:44.865+0500    DEBUG   server/server.go:366    postgres ORM engine initialization successful
...
```
