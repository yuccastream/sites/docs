---
title: Мозаика
description: Видеонаблюдение для дома и бизнеса
---

# Мозаика

Базовый функционал всех версий, его работа в платной версии не отличается от бесплатной.

## Обзор

Функция позволяет смотреть живой поток одновременно с нескольких камер.
При создании мозаики добавьте нужные камеры и сохраните её.
Также мозаику можно сделать доступной для всех пользователей на сервере.

[![mosaic](/ru/media/features/Mosaic/mosaic.png)](/ru/media/features/Mosaic/mosaic.png)

## Пример работы

<video loop autoplay muted>
  <source src="/ru/media/features/Mosaic/Mosaic.mp4" type="video/mp4">
</video>
