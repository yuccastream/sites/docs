---
title: Настройка Nginx + Let's Encrypt обратный прокси перед Yucca
description: Видеонаблюдение для дома и бизнеса
---

# Настройка Nginx + Let's Encrypt обратный прокси перед Yucca

## Введение

В этой инструкции я опишу как настроить Nginx и выпустить Let's Encrypt SSL сертификат, который будет автоматически продлеваться. Nginx будет выступать как простой Web сервер перед Yucca для терминации SSL сессии и редиректа с 80 на 443 порт.  
Зачем вообще устанавливать Nginx, ведь Yucca и сама может терминировать SSL и даже [флаги](/ru/configuration/#cert_file) есть? Это верно, но когда вы займёте 443 порт он будет недоступен для другого ПО, придется использовать какой-то другой, а так же не будет редиректа с HTTP на HTTPS – если вас это устраивает, то можете смело пропускать секцию с установкой и настройкой Nginx Шаг 1 и сразу перейти на Шаг 2.  
Все действия я буду проделывать на Ubuntu Server 22.04, но всё тоже самое будет работать и на SUSE, CentOS, Fedora, Debian и так далее. Разве что пакетный менеджер у всех будет свой, но я надеюсь вы отличите apt от zypper 🙂

## Шаг 0. Подготовка

И так нам понадобится:

1. Хост с белым IP адресом
2. Домен или поддомен который смотрит на этот адрес
3. [Установленная Yucca](/ru/install/)

В инструкции я буду использовать домен `foobar.yuccastream.com`, ваш домен будет отличаться.

```sh
nslookup foobar.yuccastream.com
Server:		127.0.0.53
Address:	127.0.0.53#53

Non-authoritative answer:
Name:	foobar.yuccastream.com
Address: 128.140.2.103
```

[![nginx1](/ru/media/other/Nginx/nginx1.png)](/ru/media/other/Nginx/nginx1.png)

Итак у меня есть Yucca, которая доступна по адресу `http://foobar.yuccastream.com:9910`

## Шаг 1. Установка и настройка Nginx

Устанавливаем nginx:

```sh
sudo apt install nginx
```

Проверяем `http://foobar.yuccastream.com`

[![nginx2](/ru/media/other/Nginx/nginx2.png)](/ru/media/other/Nginx/nginx2.png)

Далее создаём конфигурационный файл для Yucca:

```sh
sudo nano /etc/nginx/conf.d/yucca.conf
```

!!! warning "Домен `foobar.yuccastream.com` вам нужно заменить на свой"

```sh
upstream yucca_upstream {
  server 127.0.0.1:9910 fail_timeout=0;
}

server {
  listen 80;
  server_name foobar.yuccastream.com;
  location / {
    proxy_set_header Host $http_host;
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_pass http://yucca_upstream;
  }
}
```

Сохраните файл и проверьте корректность конфигурации:

```sh
sudo nginx -t
nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
nginx: configuration file /etc/nginx/nginx.conf test is successful
```

Если всё корректно вы увидите *syntax is ok*
Обновляем конфигурацию nginx:

```sh
nginx -s reload
```

Обновляем страничку и видим уже веб интерфейс Yucca `http://foobar.yuccastream.com`

[![nginx3](/ru/media/other/Nginx/nginx3.png)](/ru/media/other/Nginx/nginx3.png)

## Шаг 2. Установка Certbot и выпуск letsencrypt сертификата

Устанавливаем пакеты:

```sh
sudo apt install certbot python3-certbot-nginx
```

Выпускаем сертификат для нашего домена:

!!! warning "Домен `foobar.yuccastream.com` вам нужно заменить на свой"

```sh
sudo certbot --nginx -d foobar.yuccastream.com
```

[![nginx4](/ru/media/other/Nginx/nginx4.png)](/ru/media/other/Nginx/nginx4.png)

Сertbot попросит ввести E-mail (1), рекомендую ввести ваш реальный ящик, в случае если сертификат будет истекать и не продлиться автоматически Let's Encrypt пришлёт вам об этом уведомление. Также нужно будет согласится с условиями и правилами (2), (3). Далее certbot сам найдёт нужный конфигурационный файл с нужным доменом, настроит секцию с SSL и редирект, на выходе покажет вам ссылку.
Можете посмотреть что в итоге получилось в файле `/etc/nginx/conf.d/yucca.conf`

```sh

upstream yucca_upstream {
  server 127.0.0.1:9910 fail_timeout=0;
}

server {
  server_name foobar.yuccastream.com;
  location / {
    proxy_set_header Host $http_host;
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_pass http://yucca_upstream;
  }

    listen 443 ssl; # managed by Certbot
    ssl_certificate /etc/letsencrypt/live/foobar.yuccastream.com/fullchain.pem; # managed by Certbot
    ssl_certificate_key /etc/letsencrypt/live/foobar.yuccastream.com/privkey.pem; # managed by Certbot
    include /etc/letsencrypt/options-ssl-nginx.conf; # managed by Certbot
    ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem; # managed by Certbot
}

server {
    if ($host = foobar.yuccastream.com) {
        return 301 https://$host$request_uri;
    } # managed by Certbot

    listen 80;
    server_name foobar.yuccastream.com;
    return 404; # managed by Certbot
}
```

Рекомендую проверить, что certbot создал таймер для продления сертификата, он должен быть активен:

```sh
sudo systemctl status certbot.timer
● certbot.timer - Run certbot twice daily
     Loaded: loaded (/lib/systemd/system/certbot.timer; enabled; vendor preset: enabled)
     Active: active (waiting) since Fri 2023-09-22 11:54:02 UTC; 7min ago
    Trigger: Fri 2023-09-22 14:13:25 UTC; 2h 11min left
   Triggers: ● certbot.service

Sep 22 11:54:02 foobar systemd[1]: Started Run certbot twice daily.
```

Обновляем стринцу `http://foobar.yuccastream.com` и видим что всё работает уже через HTTPS, редирект тоже работает.

[![nginx5](/ru/media/other/Nginx/nginx5.png)](/ru/media/other/Nginx/nginx5.png)

## Шаг 3. Настройка безопасности

Мы настроили досту по HTTPS через nginx, но Yucca до сих пор доступна по HTTP на адресе http://foobar.yuccastream.com:9910. Это связанно вот с этой настройкой по умолчанию:

```sh
listen_address = ":9910"
```

Идём в конфигурационный файл Yucca и правим этот параметр:

```sh
sudo nano /opt/yucca/yucca.toml
```

Делаем так, чтобы Yucca слушала только localhost:

```sh
listen_address = "127.0.0.1:9910"
```

И перезагружаем сервер Yucca:

```sh
sudo systemctl restart yucca
```

Проверяем, теперь по адресу `http://foobar.yuccastream.com:9910` ничего нет, а тут  `https://foobar.yuccastream.com` всё работает.
