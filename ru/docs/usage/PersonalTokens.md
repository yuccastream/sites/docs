---
title: API токены
description: Видеонаблюдение для дома и бизнеса
---

# API токены

!!! info "💰 Функция доступна по [**подписке**](https://yucca.app/ru/prices) Yucca Plus или Yucca Enterprise"

## Обзор

Мы предоставляем программный интерфейс ко всем ресурсам сервера Yucca. Документация API доступна в формате SwaggerUI на любой инсталляции по адресу [http://<ваш адрес>/ui/swagger/](https://demo.yucca.app/ui/swagger/).
Авторизацию с токеном можно пройти передав его в заголовке `X-Yucca-Token`, Cookie `token` или аргументом к запросу `token`.
С персональным API токеном вы можете управлять любыми ресурсами, которые доступны вашему пользователю, кроме контента (просмотр видео) и пользовательского интерфейса.

## Быстрый старт

Перейдите в профиль пользователя, вкладку **Токены** и создайте токен на нужный период или бесконечно. Сохраните токен, повтроно посмотреть его не получится.  

[![PersonalTokens](/ru/media/features/PersonalTokens/PersonalTokens.png)](/ru/media/features/PersonalTokens/PersonalTokens.png)

## Пример с cURL

```bash
curl -X 'GET' \
  'https://demo.yucca.app/v1/annotations?all=true' \
  -H 'accept: application/json' \
  -H 'X-Yucca-Token: p.bb42c69e83915664cae70e442a349eab509890d21'
```

## Пример c SwaggerUI

<video loop autoplay muted>
  <source src="/ru/media/features/PersonalTokens/PersonalTokens.mp4" type="video/mp4">
</video>
