---
title: Квоты
description: Видеонаблюдение для дома и бизнеса
---

# Квоты

!!! info "💰 Функция доступна по [**подписке**](https://yucca.app/ru/prices) Yucca Plus или Yucca Enterprise"

## Обзор

Квоты позволяют ограничить пользователя в количестве потоков которые он может добавить и одновременных сессий.
Так же можно ограничить [глобально](/ru/configuration/#quota) на сервер количество потоков, пользователей и других ресурсов.  

[![quota1](/ru/media/features/Quota/quota1.png)](/ru/media/features/Quota/quota1.png)
