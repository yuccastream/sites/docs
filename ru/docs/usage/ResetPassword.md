---
title: Сброс пароля
description: Видеонаблюдение для дома и бизнеса
---

# Сброс пароля

## Подтверждение почты

В Yucca 0.10.0 появилась возможность сбрасывать пароль через подтверждение владения почты.
Для этого нужно, что бы у пользователя был реальный E-mail и настроенный [почтовый клиент](/ru/configuration/#smtp_client) в сервере.

[![ResetPassword1](/ru/media/other/ResetPassword/ResetPassword1.png)](/ru/media/other/ResetPassword/ResetPassword1.png)

## Командная строка

Если же почтовый клиент не настроен, то сбросить пароль любого пользователя, в том числе администратора, можно в командной строке:

```bash
/opt/yucca/yucca admin change-password --config=/opt/yucca/yucca.toml --password <NEW_PASSWDORD>
```

Если вы используете конфигурацию не через файл, то укажите свой вариант подключения к базе. 
Параметры подключения можно посмотреть в подсказке:

```bash
/opt/yucca/yucca admin change-password --help
Change a user's password

Usage:
  yucca admin change-password [flags]

Flags:
      --config string                         Path to configuration file (default "/opt/yucca/yucca.toml")
      --data-dir string                       The data directory used to store state and other persistent data (default "data")
      --database-busy-timeout int             Timeout in seconds for waiting for an SQLite table to become unlocked (default 500)
      --database-ca-cert-path string          The path to the CA certificate to use (default "/etc/ssl/certs")
      --database-cache-mode string            Shared cache setting used for connecting to the database (private, shared) (default "shared")
      --database-client-cert-path string      The path to the client cert
      --database-client-key-path string       The path to the client key
      --database-conn-max-lifetime duration   Sets the maximum amount of time a connection may be reused (default 0s)
      --database-host string                  Database host (not applicable for sqlite3) (default "127.0.0.1:5432")
      --database-max-idle-conn int            The maximum number of connections in the idle connection pool (default 2)
      --database-max-open-conn int            The maximum number of open connections to the database
      --database-name string                  The name of the Yucca database (default "yucca")
      --database-password string              The database user's password (not applicable for sqlite3) (default "postgres")
      --database-path string                  SQLite database location
      --database-ssl-mode string              SSL mode for Postgres (disable, require or verify-full) (default "disable")
      --database-user string                  The database user (not applicable for sqlite3) (default "postgres")
  -h, --help                                  Help for change-password
      --name string                           User name (leave blank to update the password for the first user)
      --password string                       User password
      --state-dir string                      The directory used to store state and other persistent data (default "<data-dir>/state")

```
