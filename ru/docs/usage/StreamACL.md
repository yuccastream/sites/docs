---
title: Контроль доступа
description: Видеонаблюдение для дома и бизнеса
---

# Контроль доступа

!!! info "💰 Функция доступна по [**подписке**](https://yucca.app/ru/prices) Yucca Plus или Yucca Enterprise"

## Обзор

Позволяет разграничивать уровень доступа пользователя к потоку.  
Уровни доступа:

- Просмотр только живого потока
- Просмотр живой поток + архив
- Редактирование потока
- Администрирование потока

[![StreamACL1](/ru/media/features/StreamACL/streamACL1.png)](/ru/media/features/StreamACL/streamACL1.png)
