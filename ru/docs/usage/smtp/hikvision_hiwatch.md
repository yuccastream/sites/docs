---
title: Настройка детекции движения на камерах Hikvision / HiWatch
description: Видеонаблюдение для дома и бизнеса
---

# Настройка детекции движения по email на камерах Hikvision / HiWatch

Откройте веб интерфейс камеры с правами администратора.

Перейдите в раздел настроек **Configuration > Network > Advanced Settings > Email**

Укажите реквизиты подключения - *SMTP Server*, *SMTP Port*, *Sender's Address* и добавьте *Receiver*. Последние 2 должны совпадать.
Если в настройках SMTP сервера включена авторизация, укажите *username* и *password*, а также включите *Authentication*
Нажмите **Test**, если всё корректно, вы увидите сообщение об успешно отправленном сообщении `Succeed`, в логах yucca сообщение о созданном событии:

```log
Event annotation created, stream_id: 10
```

а в прогресс баре соответствующей камеры появится желтый маркер.

=== "Русскоязычный интерфейс"

    [![hik_ru_01](/ru/media/other/smtp/hik_ru_01.png)](/ru/media/other/smtp/hik_ru_01.png)

=== "Англоязычный интерфейс"

    [![hik_01](/ru/media/other/smtp/hik_01.png)](/ru/media/other/smtp/hik_01.png)

[![bar_event](/ru/media/other/smtp/bar_event.png)](/ru/media/other/smtp/bar_event.png)

---

Далее нужно включить непосредственно обнаружение движения.
Перейдите в раздел настроек **Configuration > Event > Basic Event > Motion Detection**.
Включите функцию в опции *Enable Motion Detection*, во вкладке **Linkage method** включите тип уведомления *Send Email*.

=== "Русскоязычный интерфейс"

    [![hik_ru_02](/ru/media/other/smtp/hik_ru_02.png)](/ru/media/other/smtp/hik_ru_02.png)

=== "Англоязычный интерфейс"

    [![hik_02](/ru/media/other/smtp/hik_02.png)](/ru/media/other/smtp/hik_02.png)
